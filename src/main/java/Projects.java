import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Projects extends Commands {

    public Projects (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void openProject(){
        click("//a[text()='"+projectName+"']");
        waitVisibilityOfElement("//h1[text()='"+projectName+"']");
    }

    public void makeNewProject(){
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText(nameField, projectName);
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "2");
        click(createButton);
        waitPresenceOfElement("//a[text()='"+projectName+"']");
    }

    public boolean newProjectMade() {
        boolean result = false;
        if (!isDisplayed("//a[text()='"+projectName+"']")) {  }
            else {
            click("//a[text()='"+projectName+"']/..//..//..//button[@class='styled_toggle__6Bo8e']");
            click(editButton);
            waitPresenceOfElement("//h1[text()='Edit project']");
            if (getAttributeValue(nameField).equals(projectName) &&
                    getAttributeValue("//input[@placeholder='Latitude']").equals("1") &&
                    getAttributeValue("//input[@placeholder='Longitude']").equals("2"))
                    {result = true;}
            }
        click(cancelButton);
        return result;
    }

    public boolean blankNameFieldErrorDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText(nameField, "");
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "2");
        click(createButton);
        return isDisplayed(dataRequired);
    }

    public boolean blankLatitudeFieldErrorDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText(nameField, projectName);
        sendText("//input[@placeholder='Latitude']", "");
        sendText("//input[@placeholder='Longitude']", "2");
        click(createButton);
        return isDisplayed(dataRequired);
    }

    public boolean blankLongitudeFieldErrorDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText(nameField, projectName);
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "");
        click(createButton);
        return isDisplayed(dataRequired);
    }

    public boolean latitudeLess90ErrorMessageDisplayed()  {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText("//input[@placeholder='Latitude']", "-91");
        sendText("//input[@placeholder='Longitude']", "2");
        click(createButton);
        return isDisplayed("//span[text()='Minimum value is -90']");
    }

    public boolean latitudeMore90ErrorMessageDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText("//input[@placeholder='Latitude']", "91");
        sendText("//input[@placeholder='Longitude']", "2");
        click(createButton);
        return isDisplayed("//span[text()='Maximum value is 90']");
    }

    public boolean longitudeLess180ErrorDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "-181");
        click(createButton);
        return isDisplayed("//span[text()='Minimum value is -180']");
    }

    public boolean longitudeMore180ErrorMessageDisplayed() {
        click("//a[text()='"+clientName+"']");
        click(createNewButton);
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "182");
        click(createButton);
        return isDisplayed("//span[text()='Maximum value is 180']");
    }

    public boolean editProjectCheck() {
        boolean result = false;
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText(nameField, "1");
        sendText("//input[@placeholder='Latitude']", "1");
        sendText("//input[@placeholder='Longitude']", "1");
        click(saveButton);
        waitPresenceOfElement("//a[text()='"+projectName+"1']");
        if (!isDisplayed("//a[text()='"+projectName+"1']")) {}
            else {
            click("//a[text()='"+projectName+"1']/..//..//..//button[@class='styled_toggle__6Bo8e']");
            click(editButton);
            if (getAttributeValue("//input[@placeholder='Latitude']").equals("11") &&
                    getAttributeValue("//input[@placeholder='Longitude']").equals("21")) {result = true;}
            click(cancelButton);
            }
        return result;
    }

    public boolean editBlankNameFieldErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText(nameField, clearValue);
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editBlankLatitudeFieldErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Latitude']", clearValue);
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editBlankLongitudeFieldErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Longitude']", clearValue);
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editProjectLatitudeLess90ErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Latitude']", (replaceValue("-91")));
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editProjectLatitudeMore90ErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Latitude']", (replaceValue("91")));
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editProjectLongitudeLess180ErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Longitude']", replaceValue("-181"));
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean editProjectLongitudeMore180ErrorDisplayed() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click(editButton);
        sendText("//input[@placeholder='Longitude']", replaceValue("181"));
        click(saveButton);
        return isDisplayed(dataRequired);
    }

    public boolean projectDeleted() {
        click("//a[text()='"+projectName+"']/..//..//button[@class='styled_toggle__6Bo8e']");
        click("//a[text()='Delete']");
        click("//span[text()='Delete']");
        waitStalenessOfElement("//a[text()='autoTest Project new']");
        return findElementsSize("//a[text()='autoTest1']") == 0;
    }

    public boolean projectFromStructureDeleted() {
        click("//a[text()='Structures']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditStructureDeleted() {
        click("//a[text()='Structures']");
        waitVisibilityOfElement("//p[text()='"+structureName+"']");
        click("//p[text()='"+structureName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromCropDeleted() {
        click("//a[text()='Crops']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditCropDeleted() {
        click("//a[text()='Crops']");
        waitVisibilityOfElement("//p[text()='"+cropName+"']");
        click("//p[text()='"+cropName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromDatasetDeleted() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditWeatherDatasetDeleted() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//p[text()='"+weatherDatasetName+"']");
        click("//p[text()='"+weatherDatasetName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditProductionDatasetDeleted() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//p[text()='"+productionDatasetName+"']");
        click("//p[text()='"+productionDatasetName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromSimulationDeleted() {
        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditSimulationDeleted() {
        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//p[text()='"+simulationName+"']");
        click("//p[text()='"+simulationName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromSimulationResultDeleted() {
        waitVisibilityOfElement("//a[text()='Simulations']/../button");
        click("//a[text()='Simulations']/../button");
        waitVisibilityOfElement("//a[contains(text(),'"+simulationName+" Result')]");
        click("//a[contains(text(),'"+simulationName+" Result')]");
        waitVisibilityOfElement("//h2[contains(text(),'"+simulationName+" Result')]");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromBatchDeleted() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromEditBatchDeleted() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//p[text()='"+batchName+"']");
        click("//p[text()='"+batchName+"']");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromBatchResultDeleted() {
        waitVisibilityOfElement("//a[text()='Batches']/../button");
        click("//a[text()='Batches']/../button");
        waitVisibilityOfElement("//a[contains(text(),'"+batchName+" Result')]");
        click("//a[contains(text(),'"+batchName+" Result')]");
        waitVisibilityOfElement("//h2[contains(text(),'"+batchName+" Result')]");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean projectFromBatchCalculationDeleted() {
        waitVisibilityOfElement("//a[text()='Batches']/../button");
        click("//a[text()='Batches']/../button");
        //waitVisibilityOfElement("//a[contains(text(),'"+batchName+" Result')]");
        waitVisibilityOfElement("//a[text()='Batches']/../..//a[contains(text(),'"+simulationName+"')]");
        //click("//a[contains(text(),'"+batchName+" Result')]");
        click("//a[text()='Batches']/../..//a[contains(text(),'"+simulationName+"')]");
        waitVisibilityOfElement("//h2[contains(text(),'"+batchName+" - "+batchName+ " Results')]");
        waitVisibilityOfElement("//button//*[text()='Delete Project']");
        click("//button//*[text()='Delete Project']");
        waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
        click("//div[@class='modal-content']" + deleteButton);
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return findElementsSize("//a[text()='"+projectName+"']") == 0;
    }

    public boolean clientNameDisplayed() {
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        click("//a[text()='"+clientName+"']");
        waitVisibilityOfElement("//h1[text()='"+clientName+"']");
        return isDisplayed("//h1[text()='"+clientName+"']");
    }

    public boolean clientDescriptionDisplayed() {
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        click("//a[text()='"+clientName+"']");
        waitVisibilityOfElement("//p[text()='"+clientDescription+"']");
        return isDisplayed("//p[text()='"+clientDescription+"']");
    }

    public void makeMultipleProjects(){
        click("//a[text()='"+clientName+"']");
        for (int i=0; i<4; i++){
            click(createNewButton);
            sendText(nameField, projectName+i);
            sendText("//input[@placeholder='Latitude']", "1");
            sendText("//input[@placeholder='Longitude']", "2");
            click(createButton);
            waitVisibilityOfElement("//a[text()='"+projectName+i+"']");
        }
    }

    public void sortingNameAscending() {
        click("//button[text()='Sort by']");
        click("//span[text()='Name']");
        waitStalenessOfElement("//div[@role='status']");
    }

    public boolean projectsNameSortedAscending() {
        boolean result = false;
        for (int i=1; i < findElementsSize("//a[@class='styled_name__m7pBk']"); i++) {
            if ((getText("(//a[@class='styled_name__m7pBk'])[" + i + "]").compareToIgnoreCase(
                    getText("(//a[@class='styled_name__m7pBk'])[" + (i + 1) + "]"))) <= 0) {result = true;}
            else {result = false; i= findElementsSize("//a[@class='styled_name__m7pBk");}
        }
        return result;
    }

    public void sortingNameDescending() {
        click("//button[text()='Sort by']");
        click("(//span[text()='Name'])[2]");
        waitStalenessOfElement("//div[@role='status']");
    }

    public boolean projectsNameSortedDescending() {
        boolean result = false;
        for (int i=1; i < findElementsSize("//a[@class='styled_name__m7pBk']"); i++) {
            if ((getText("(//a[@class='styled_name__m7pBk'])[" + i + "]").compareToIgnoreCase(
                    getText("(//a[@class='styled_name__m7pBk'])[" + (i + 1) + "]"))) >= 0) {result = true;}
            else {result = false; i= findElementsSize("//a[@class='styled_name__m7pBk']");}
        }
        return result;
    }

}