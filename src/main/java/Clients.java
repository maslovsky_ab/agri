import org.apache.commons.lang3.text.WordUtils;
import org.json.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;

public class Clients extends Commands {
    API api = new API();
    String emailField = "//input[@name='email']";
    String descriptionField = "//textarea[@name='description']";

    public Clients (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void makeNewClient() {
        click(createNewButton);
        sendText(nameField, clientName);
        sendText(emailField, clientEmail);
        sendText(descriptionField, clientDescription);
        click(createButton);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
    }

    public boolean newClientCreated() {
        boolean result = false;
        if (!isDisplayed("//a[text()='"+clientName+"']")) { }
        else {
            click("//a[text()='"+clientName+"']/..//..//..//button");
            click(editButton);
            waitPresenceOfElement("//h1[text()='Edit client']");
            if (getAttributeValue(nameField).equals(clientName) &&
                    getAttributeValue(emailField).equalsIgnoreCase(clientEmail) &&
                    getText(descriptionField).equals(clientDescription)) {result = true;}
            }
        click("//button[@class='styled_button__MiVhp styled_text__BX30s']");
        return result;
    }

    public void makeNewClientWithBlankNameField() {
        click(createNewButton);
        sendText(nameField, "");
        sendText(emailField, clientEmail);
        sendText(descriptionField, clientDescription);
        click(createButton);
    }

    public boolean errorMessageBlankNameFieldDisplayed() {
        return isDisplayed(dataRequired);
    }

    public void makeNewClientWithLongName() {
        click(createNewButton);
        sendText(nameField, longName);
        sendText(emailField, clientEmail);
        sendText(descriptionField, clientDescription);
        click(createButton);
    }

    public boolean errorMessageVeryLongNameFieldDisplayed() {
        return isDisplayed(longNameError);
    }

    public void makeNewClientWithBlankEmailField() {
        click(createNewButton);
        sendText(nameField, clientName);
        sendText(emailField, "");
        sendText(descriptionField, clientDescription);
        click(createButton);
    }

    public boolean errorMessageBlankEmailFieldDisplayed() {
        return isDisplayed(dataRequired);
    }

    public void makeNewClientWithMisspelledEmail(){
        click(createNewButton);
        sendText(nameField, clientName);
        sendText(emailField, "1#1.com");
        sendText(descriptionField, clientDescription);
        click(createButton);
    }

    public boolean errorMessageMisspelledEmailFieldDisplayed() {
        return isDisplayed("//span[text()='email must be a valid email']");
    }

    public void editClient() {
        click("//a[text()='"+clientName+"']/..//..//..//button");
        click(editButton);
        sendText(nameField, "1");
        sendText(descriptionField, "1");
        click(saveButton);
        waitVisibilityOfElement("//a[text()='"+clientName+"1']");
        }

    public boolean editedDataSaved() {
        boolean result = false;
        if (!isDisplayed("//a[text()='"+clientName+"1']")) { }
        else {
            click("//a[text()='"+clientName+"1']/..//..//..//button");
            click(editButton);
            waitPresenceOfElement("//h1[text()='Edit client']");
            if (getAttributeValue(nameField).equals(clientName+"1") &&
                    getText(descriptionField).equals(clientDescription+"1") ) {result = true;}
            }
            click(cancelButton);
        waitVisibilityOfElement("//a[text()='"+clientName+"1']");
        return result;
        }

    public void deleteClient() {
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        click("//*[contains(text(),'"+clientName+"')]/..//..//..//button");
        click(deleteButton);
        waitVisibilityOfElement("//div[@class='modal-content']"+deleteButton);
        click("//div[@class='modal-content']"+deleteButton);
    }

    public boolean clientDeleted(){
        waitStalenessOfElement("//*[contains(text(),'"+clientName+"')]");
        return findElementsSize("//*[contains(text(),'"+clientName+"')]") == 0;
    }

    public void makeMultipleClients(){
        for (int i=0; i<4; i++){
            click(createNewButton);
            sendText(nameField, clientName+i);
            sendText(emailField, clientEmail);
            sendText(descriptionField, clientDescription);
            click(createButton);
            waitVisibilityOfElement("//a[text()='"+clientName+i+"']");
        }

    }

    public void sortingNameAscending() {
        click("//button[text()='Sort by']");
        click("//span[text()='Name']");
        waitStalenessOfElement("//div[@role='status']");
    }

    public boolean clientsNameSortedAscending() {
        boolean result = false;
        for (int i=1; i < findElementsSize("//a[@class='styled_name__Syg0k']"); i++) {
            if ((getText("(//a[@class='styled_name__Syg0k'])[" + i + "]").compareToIgnoreCase(
                    getText("(//a[@class='styled_name__Syg0k'])[" + (i + 1) + "]"))) <= 0) {result = true;}
            else {result = false; i= findElementsSize("//a[@class='styled_name__Syg0k']");}
        }
        return result;
    }

    public void sortingNameDescending() {
        click("//button[text()='Sort by']");
        click("(//span[text()='Name'])[2]");
        waitStalenessOfElement("//div[@role='status']");
    }

    public boolean clientsNameSortedDescending() {
        boolean result = false;
        for (int i=1; i < findElementsSize("//a[@class='styled_name__Syg0k']"); i++) {
            if ((getText("(//a[@class='styled_name__Syg0k'])[" + i + "]").compareToIgnoreCase(
                    getText("(//a[@class='styled_name__Syg0k'])[" + (i + 1) + "]"))) >= 0) {result = true;}
            else {result = false; i= findElementsSize("//a[@class='styled_name__Syg0k']");}
        }
        return result;
    }

    /*public void sortingDateAscending() throws JSONException, IOException {
        click("//button[text()='Sort by']");
        click("(//span[text()='Date'])[2]");
        waitStalenessOfElement("//div[@role='status']");
    }

    public boolean clientsDateSortedAscending() throws JSONException, IOException {
        JSONArray jsa = api.getClientsList().getJSONArray("data");
        boolean result = false;
        String person;
        String date="";

        for (int i=1; i < findElementsSize("//a[@class='styled_name__Syg0k']")+1; i++){
            person = getText("(//a[@class='styled_name__Syg0k'])["+i+"]");
            for (int j=0; j < findElementsSize("//a[@class='styled_name__Syg0k']"); j++) {
                if (person.equalsIgnoreCase(jsa.getJSONObject(j).getString("name")))
                    {date = jsa.getJSONObject(j).getString("created_at");
                        System.out.println(jsa.getJSONObject(j).getString("name"));
                        System.out.println(date);
                    }
            }
        }
    System.out.println(jsa);
        return result;
    }*/

    public void deleteMultipleClients() {
        for (int i = 0; i < 4; i++) {
            click("//*[text()='"+clientName+i+"']/..//..//..//button");
            waitVisibilityOfElement(deleteButton);
            click(deleteButton);
            waitVisibilityOfElement("//div[@class='modal-content']" + deleteButton);
            click("//div[@class='modal-content']" + deleteButton);
            waitStalenessOfElement("//div[@class='modal-content']" + deleteButton);
        }
    }

    public boolean numberOfProjectsDisplayed() {
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        click("//a[text()='"+clientName+"']");
        waitVisibilityOfElement("//main//a");
        int numberProjects = findElementsSize("//main//a");
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        return (getText("//a[text()='"+clientName+"']/../span").equalsIgnoreCase(numberProjects+" Projects"));
    }

    public boolean userNameIconDisplayed() {
        String text = WordUtils.initials(clientName);
        driver.get(clientsURL);
        waitVisibilityOfElement("//a[text()='"+clientName+"']");
        if (text.length() == 1) text = text.substring(0,1);
        else text = text.substring(0,2);
        return (text.equalsIgnoreCase(getText("//a[text()='"+clientName+"']/../../div")));
    }

}