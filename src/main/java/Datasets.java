import com.codeborne.selenide.selector.ByText;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;

public class Datasets extends Commands {

    public Datasets (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void makeNewWeatherDataset() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//h2[text()='Datasets']");
        click(createNewButton);
        waitPresenceOfElement("//label[text()='Dataset type']");
        sendText(nameField, weatherDatasetName);
        click("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']");
        sendText("//label[text()='Dataset type']/..//input", "Weather"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='Weather']");

        click("//label[text()='Sampling rate']/..//div[@class='select__value-container select__value-container--has-value css-1olco0v']");
        sendText("//label[text()='Sampling rate']/..//input", "1 hour"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='1 hour']");

        click("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']");
        sendText("//label[text()='Timezone']/..//input", "Etc/GMT0"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='Etc/GMT0']");

        File file = new File("src/main/resources/Datasets.xlsx");
        sendText("//input[@type='file']", file.getAbsolutePath());
        click(saveButton);
        waitVisibilityOfElement("//h3[text()='Weather Dataset']/..//p[text()='"+weatherDatasetName+"']");
    }

    public boolean newWeatherDatasetDataDisplayed() {
        boolean result = false;
        if (isDisplayed("//h3[text()='Weather Dataset']/..//p[text()='"+weatherDatasetName+"']")) {
            click("//h3[text()='Electrical Production Datasets']/..//button[@class='DatasetsCard_button__1nmIx']");
            waitVisibilityOfElement("//label[text()='Dataset File']/..//p[text()='Datasets']");
            result = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Weather") &&
                    getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("1 hour") &&
                    getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Etc/GMT0") &&
                    getText("//label[text()='Dataset File']/..//p").equals("Datasets");
        }
        return result;
    }

    public void makeNewProductionDataset() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//h2[text()='Datasets']");
        click(createNewButton);
        waitPresenceOfElement("//label[text()='Dataset type']");
        sendText(nameField, productionDatasetName);
        click("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']");
        waitVisibilityOfElement("//*[text()='Production']");
        click("//*[text()='Production']");
        //driver.findElement(By.xpath("//label[text()='Dataset type']/..//input")).sendKeys("Production", Keys.RETURN);
        //sendText("//label[text()='Dataset type']/..//input", "Production"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='Production']");

        click("//label[text()='Sampling rate']/..//div[@class='select__value-container select__value-container--has-value css-1olco0v']");
        sendText("//label[text()='Sampling rate']/..//input", "1 hour"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='1 hour']");

        click("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']");
        sendText("//label[text()='Timezone']/..//input", "Etc/GMT0"+Keys.RETURN);
        waitVisibilityOfElement("//div[text()='Etc/GMT0']");

        File file = new File("src/main/resources/Datasets.xlsx");
        sendText("//input[@type='file']", file.getAbsolutePath());
        click(saveButton);
        waitVisibilityOfElement("//h3[text()='Weather Dataset']/..//p[text()='"+productionDatasetName+"']");
    }

    public boolean newProductionDatasetDataDisplayed() {
        boolean result = false;
        if (isDisplayed("//h3[text()='Electrical Production Datasets']/..//p[text()='"+productionDatasetName+"']")) {
            click("//h3[text()='Electrical Production Datasets']/..//button[@class='DatasetsCard_button__1nmIx']");
            waitVisibilityOfElement("//label[text()='Dataset File']/..//p[text()='Datasets']");
            result = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Production") &&
                    getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("1 hour") &&
                    getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Etc/GMT0") &&
                    getText("//label[text()='Dataset File']/..//p").equals("Datasets");
        }
        return result;
    }

    public void makeNewBlankDataset() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//h2[text()='Datasets']");
        click(createNewButton);
        waitPresenceOfElement("//label[text()='Dataset type']");
        click(saveButton);
    }

    public boolean blankDatasetErrorDisplayed() {
        boolean result =(findElementsSize("//*[text()='This field is required']")) == 3 &&
                isDisplayed("//*[text()='Dataset file is required']");
        click(cancelButton);
        return result;
    }

    public void makeNewDatasetIncorrectExcel() {
        click("//a[text()='Datasets']");
        waitVisibilityOfElement("//h2[text()='Datasets']");
        click(createNewButton);
        waitPresenceOfElement("//label[text()='Dataset type']");
        sendText(nameField, weatherDatasetName);
        click("//label[text()='Dataset type']/..//div[@class='select__placeholder css-1jqq78o-placeholder']");
        driver.findElement(new ByText("Weather")).click();
        click("//label[text()='Sampling rate']/..//div[@class='select__value-container select__value-container--has-value css-1olco0v']");
        driver.findElement(new ByText("1 hour")).click();
        click("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']");
        driver.findElement(new ByText("Etc/GMT0")).click();
        File file = new File("src/main/resources/IncorrectDatasets.xlsx");
        sendText("//input[@type='file']", file.getAbsolutePath());
        click(saveButton);
        waitVisibilityOfElement("//h3[text()='Error']");
    }

    public boolean IncorrectExcelErrorMessageDisplayed() {
        boolean result = isDisplayed("//h3[text()='Error']");
        click(cancelButton);
        return result;
    }

    public void editWeatherDataset() {
        click("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']");
        waitVisibilityOfElement("//label[text()='Dataset type']");
        sendText(nameField, replaceValue(productionDatasetName));
        click("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
        driver.findElement(new ByText("Production")).click();
        click("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']");
        driver.findElement(new ByText("Etc/GMT+1")).click();
        click("//button[@class='DatasetModel_delete-file__+xNa9']");
        waitVisibilityOfElement("//p[text()='Drop your Excel file to upload']");
        File file = new File("src/main/resources/Datasets4replace.xlsx");
        sendText("//input[@type='file']", file.getAbsolutePath());
        click(saveButton);
        waitVisibilityOfElement("//h3[text()='Weather Dataset']/..//p[text()='"+productionDatasetName+"']");
    }

    public boolean editedWeatherDatasetValuesSaved() {
        boolean result = false;
        if (isDisplayed("//h3[text()='Weather Dataset']/..//p[text()='"+productionDatasetName+"']")) {
            click("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']");
            waitVisibilityOfElement("//label[text()='Dataset type']");
            result = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Production") &&
                    getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Etc/GMT+1") &&
                    getText("//label[text()='Dataset File']/..//p").equals("Datasets4replace");
        }
        return result;
    }

    public void editProductionDataset() {
        click("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']");
        waitVisibilityOfElement("//label[text()='Dataset type']");
        sendText(nameField, replaceValue(weatherDatasetName));
        click("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
        driver.findElement(new ByText("Weather")).click();
        click("//label[text()='Timezone']/..//div[@class=' css-uq3nfk-container']");
        driver.findElement(new ByText("Etc/GMT+1")).click();
        click("//button[@class='DatasetModel_delete-file__+xNa9']");
        waitVisibilityOfElement("//p[text()='Drop your Excel file to upload']");
        File file = new File("src/main/resources/Datasets4replace.xlsx");
        sendText("//input[@type='file']", file.getAbsolutePath());
        click(saveButton);
        waitVisibilityOfElement("//h3[text()='Weather Dataset']/..//p[text()='"+weatherDatasetName+"']");
    }

    public boolean editedProductionDatasetValuesSaved() {
        boolean result = false;
        if (isDisplayed("//h3[text()='Weather Dataset']/..//p[text()='"+weatherDatasetName+"']")) {
            click("//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx']");
            waitVisibilityOfElement("//label[text()='Dataset type']");
            result = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Weather") &&
                    getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("Etc/GMT+1") &&
                    getText("//label[text()='Dataset File']/..//p").equals("Datasets4replace");
        }
        return result;
    }

    public boolean duplicatedDatasetSame() {
        click("(//h3[text()='Weather Dataset']/..//button[@class='DatasetsCard_button__1nmIx'])[2]");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='DatasetsCard_container__2q18I']"))).getSize().equals(2);
        boolean result = false;
        if (getText("(//div[@class='DatasetsCard_container__2q18I']//p)[1]").equals
                (getText("(//div[@class='DatasetsCard_container__2q18I']//p)[2]"))){
            click("(//button[@class='DatasetsCard_button__1nmIx'])[1]");
            waitVisibilityOfElement("//p[text()='Datasets']");
            String datasetType = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
            String samplingRate = getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
            String timezone = getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
            String datasetName = getText("//label[text()='Dataset File']/..//p");
            click(cancelButton);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='DatasetsCard_container__2q18I']"))).getSize().equals(2);
            click("(//button[@class='DatasetsCard_button__1nmIx'])[3]");
            waitVisibilityOfElement("//p[text()='Datasets']");
            result = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals(datasetType) &&
                    getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals(samplingRate) &&
                    getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals(timezone) &&
                    getText("//label[text()='Dataset File']/..//p").equals(datasetName);
        }
        return result;
    }

    public boolean datasetDeleted(){
        click("//button[@class='DatasetsCard_button__1nmIx DatasetsCard_red__zQUj7']");
        click(deleteButton);
        waitStalenessOfElement("//div[@class='DatasetsCard_container__2q18I']");
        return findElementsSize("//div[@class='DatasetsCard_container__2q18I']") == 0;
    }

}