import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.io.File.separator;

public class Batch extends Commands {
    String panelHeight, panelOpacity, azimuth, panelSizeX, panelSizeY, initialOffsetX, initialOffsetY;
    String panelSize, initialOffset, numberOfPanels, spaceBetweenPanels, fieldSize, structureType, panelTilt;
    String panelTranslation , maximalLateralTranslation, lateralOffset, startingPoint, endingPoint;
    String weatherDatasetType, weatherSamplingRate, weatherTimezone, prodDatasetType, prodSamplingRate, prodTimezone;
    String numberOfPanelsX, numberOfPanelsY, spaceBetweenPanelsX,spaceBetweenPanelsY, fieldSizeX, fieldSizeY;

    public Batch (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void makeNewBatch()  {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//h2[text()='Batches']");
        click(createNewButton);
        waitVisibilityOfElement("//div[text()='30 minutes']");
        sendText(nameField, batchName);
        sendText("//label[text()='Base Simulation']/..//input", simulationName+Keys.RETURN);
        sendText("//label[text()='Simulation Frequency']/..//input", "1 hour"+Keys.RETURN);
        waitVisibilityOfElement("//label[text()='Parameters of interest']/../div");
        click("//label[text()='Parameters of interest']/../div");
        waitVisibilityOfElement("//h3[text()='Datasets']");
        click("//h3[text()='Structures']");
        waitVisibilityOfElement("//label[text()='Panel Height']");
        click("//label[text()='Panel Height']");
        click("//label[text()='Panel Opacity']");
        click("//label[text()='Azimuth']");
        click("//label[text()='Panel Size']");
        click("//label[text()='Panel Offset']");
        click("//label[text()='Number of Panels']");
        click("//label[text()='Space Between Panels']");
        click("//label[text()='Field Size']");

        click("//h3[text()='Crops']");
        waitVisibilityOfElement("//label[text()='Crop']");
        click("//label[text()='Crop']");

        click("//h3[text()='Datasets']");
        waitVisibilityOfElement("//label[text()='Weather Dataset']");
        click("//label[text()='Weather Dataset']");
        click("//label[text()='Production Dataset']");

        click("//*[text()='Add row']");
        click(saveButton);
        waitStalenessOfElement("//span[@class='styled_label__gZBu7 styled_isLoading__U86n+']");
        click(cancelButton);
        waitVisibilityOfElement("//p[text()='"+batchName+"']");
    }

    public boolean newBatchCreatedCorrectly() {
        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement("//input[@name='rows.0.panel_height']");

        simulationName = getText("(//label[text()='Base Simulation']/..//div)[4]");
        panelHeight = getAttributeValue("//input[@name='rows.0.panel_height']");
        panelOpacity = getAttributeValue("//input[@name='rows.0.panel_opacity']");
        azimuth = getAttributeValue("//input[@name='rows.0.azimuth']");
        panelSizeX = getAttributeValue("//input[@name='rows.0.panel_size.x']");
        panelSizeY = getAttributeValue("//input[@name='rows.0.panel_size.y']");
        initialOffsetX = getAttributeValue("//input[@name='rows.0.initial_offset.x']");
        initialOffsetY = getAttributeValue("//input[@name='rows.0.initial_offset.y']");
        numberOfPanelsX = getAttributeValue("//input[@name='rows.0.panels_number.x']");
        numberOfPanelsY = getAttributeValue("//input[@name='rows.0.panels_number.y']");
        spaceBetweenPanelsX = getAttributeValue("//input[@name='rows.0.panels_gap.x']");
        spaceBetweenPanelsY = getAttributeValue("//input[@name='rows.0.panels_gap.y']");
        fieldSizeX = getAttributeValue("//input[@name='rows.0.field_size.x']");
        fieldSizeY = getAttributeValue("//input[@name='rows.0.field_size.x']");
        weatherDatasetName = getText("(//td[9]//div)[4]");
        productionDatasetName = getText("(//td[10]//div)[4]");
        cropName = getText("(//td[11]//div)[4]");

        //////////// сравниваем с данными из Симуляции и Структуры /////////////////////////////
        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//p[text()='"+simulationName+"']");
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Timezone']");
        structureName = getText("(//span[text()='Structures']/..//div)[4]");
        boolean result = false;
        if (getText("(//span[text()='Crops']/..//div)[4]").equals(cropName) &&
            getText("(//span[text()='Weather Dataset']/..//div)[4]").equals(weatherDatasetName) &&
            getText("(//span[text()='Electrical Production Dataset']/..//div)[4]").equals(productionDatasetName)) {
                click("//a[text()='Structures']");
                waitVisibilityOfElement("//p[text()='"+structureName+"']");
                click("//button[@class='styled_button__e6WEB']");
                waitVisibilityOfElement("//span[text()='Draw']");
                result = getAttributeValue("//input[@name='panel_height']").equals(panelHeight) &&
                    getText("(//label[text()='Panel Opacity']/..//div)[4]").equals(panelOpacity) &&
                    getText("(//label[text()='Azimuth']/..//div)[4]").equals(azimuth) &&
                    getAttributeValue("//input[@name='panel_x']").equals(panelSizeX) &&
                    getAttributeValue("//input[@name='panel_y']").equals(panelSizeY) &&
                    getAttributeValue("//input[@name='initial_offset_x']").equals(initialOffsetX) &&
                    getAttributeValue("//input[@name='initial_offset_y']").equals(initialOffsetY) &&
                    getAttributeValue("//input[@name='panels_number_x']").equals(numberOfPanelsX) &&
                    getAttributeValue("//input[@name='panels_number_y']").equals(numberOfPanelsY) &&
                    getAttributeValue("//input[@name='panels_gap_x']").equals(spaceBetweenPanelsX) &&
                    getAttributeValue("//input[@name='panels_gap_y']").equals(spaceBetweenPanelsY) &&
                    getAttributeValue("//input[@name='field_size_x']").equals(fieldSizeX) &&
                    getAttributeValue("//input[@name='field_size_y']").equals(fieldSizeY);
        }
        return result;
    }

    public void editBatch() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr']");

        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement("//span[text()='Panel Height']");
        sendText("//input[@name='rows.0.panel_height']", replaceValue("5"));

        click(saveButton);
        waitVisibilityOfElement("//p[text()='Batch']");
    }

    public boolean editedBatchSaved() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr']");
        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement("//span[text()='Panel Height']");
        return(getAttributeValue("//input[@name='rows.0.panel_height']").equals("5") &&
                driver.findElement(By.xpath("//input[@name='rows.0.panel_height']/../../../../td"))
                        .getCssValue("background-color").equals("rgba(224, 224, 224, 1)"));
    }

    public void duplicateBatch() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr']");
        click("(//button[@class='styled_button__Y2Knr'])[2]");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__mwg4x']"), 1));
    }

    public boolean batchDuplicatedCorrectly() {
        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement("//span[text()='Panel Height']");

        //////////// читаем данные из оригинального Batch /////////////////////////////
        simulationName = getText("(//label[text()='Base Simulation']/..//div)[4]");
        panelHeight = getAttributeValue("//input[@name='rows.0.panel_height']");
        panelOpacity = getAttributeValue("//input[@name='rows.0.panel_opacity']");
        azimuth = getAttributeValue("//input[@name='rows.0.azimuth']");
        panelSizeX = getAttributeValue("//input[@name='rows.0.panel_size.x']");
        panelSizeY = getAttributeValue("//input[@name='rows.0.panel_size.y']");
        initialOffsetX = getAttributeValue("//input[@name='rows.0.initial_offset.x']");
        initialOffsetY = getAttributeValue("//input[@name='rows.0.initial_offset.y']");
        numberOfPanelsX = getAttributeValue("//input[@name='rows.0.panels_number.x']");
        numberOfPanelsY = getAttributeValue("//input[@name='rows.0.panels_number.y']");
        spaceBetweenPanelsX = getAttributeValue("//input[@name='rows.0.panels_gap.x']");
        spaceBetweenPanelsY = getAttributeValue("//input[@name='rows.0.panels_gap.y']");
        fieldSizeX = getAttributeValue("//input[@name='rows.0.field_size.x']");
        fieldSizeY = getAttributeValue("//input[@name='rows.0.field_size.x']");
        weatherDatasetName = getText("(//td[9]//div)[4]");
        productionDatasetName = getText("(//td[10]//div)[4]");
        cropName = getText("(//td[11]//div)[4]");

        click("//a[text()='Batches']");
        waitVisibilityOfElement("(//button[@class='styled_button__Y2Knr'])[3]");
        click("(//button[@class='styled_button__Y2Knr'])[3]");
        waitVisibilityOfElement("//span[text()='Panel Height']");
        return(getText("(//label[text()='Base Simulation']/..//div)[4]").equals(simulationName) &&
                getAttributeValue("//input[@name='rows.0.panel_height']").equals(panelHeight) &&
                getAttributeValue("//input[@name='rows.0.panel_opacity']").equals(panelOpacity) &&
                getAttributeValue("//input[@name='rows.0.azimuth']").equals(azimuth) &&
                getAttributeValue("//input[@name='rows.0.panel_size.x']").equals(panelSizeX) &&
                getAttributeValue("//input[@name='rows.0.panel_size.y']").equals(panelSizeY) &&
                getAttributeValue("//input[@name='rows.0.initial_offset.x']").equals(initialOffsetX) &&
                getAttributeValue("//input[@name='rows.0.initial_offset.y']").equals(initialOffsetY) &&
                getAttributeValue("//input[@name='rows.0.panels_number.x']").equals(numberOfPanelsX) &&
                getAttributeValue("//input[@name='rows.0.panels_number.y']").equals(numberOfPanelsY) &&
                getAttributeValue("//input[@name='rows.0.panels_gap.x']").equals(spaceBetweenPanelsX) &&
                getAttributeValue("//input[@name='rows.0.panels_gap.y']").equals(spaceBetweenPanelsY) &&
                getAttributeValue("//input[@name='rows.0.field_size.x']").equals(fieldSizeX) &&
                getAttributeValue("//input[@name='rows.0.field_size.x']").equals(fieldSizeY) &&
                getText("(//td[9]//div)[4]").equals(weatherDatasetName) &&
                getText("(//td[10]//div)[4]").equals(productionDatasetName) &&
                getText("(//td[11]//div)[4]").equals(cropName));
    }

    public void deleteBatch() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr styled_red__DzZvD']");
        click("//button[@class='styled_button__Y2Knr styled_red__DzZvD']");
        waitVisibilityOfElement("//div[@class='modal-content']"+deleteButton);
        click("//div[@class='modal-content']"+deleteButton);
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__mwg4x']"), 2));
    }

    public void deleteBatchFromEdit() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr']");
        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement(deleteButton);
        click(deleteButton);
        waitVisibilityOfElement("//div[@class='modal-content']"+deleteButton);
        click("//div[@class='modal-content']"+deleteButton);
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__mwg4x']"), 2));
    }

    public boolean batchDeleted() {
        return (findElementsSize("//div[@class='styled_container__mwg4x']") < 2);
    }

    public void runBatch() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//button[@class='styled_button__Y2Knr']");
        click("//button[@class='styled_button__Y2Knr']");
        waitVisibilityOfElement("//span[text()='Run Simulation']");
        click("//span[text()='Run Simulation']");
        waitVisibilityOfElement("//div[text()='Running']");
        click("//a[text()='Batches']/../button[@class='styled_toggle__yOoJv']");
        waitVisibilityOfElement("//a[text()='"+simulationName+" 1']");
        click("//a[text()='"+simulationName+" 1']");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Timezone']")));

        //////////////////////////////////////////////////////////////////////////////
        int i = 1;
        while (i != 0) {
            try { findElement("//div[@class='styled_spinner__-A5lA spinner-border']").isDisplayed();}
            catch (Exception e) {i = 0;}
            }
        //////////////////////////////////////////////////////////////////////////////
    }

    public void startBatchCalculation() {
        click("//a[text()='Batches']");
        waitVisibilityOfElement("//p[text()='" + batchName + "']");
        click("//p[text()='" + batchName + "']");
        waitVisibilityOfElement("//span[text()='Run Simulation']");
        waitVisibilityOfElement("//button//span[text()='Run Simulation']");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Run Simulation']")));
        click("//span[text()='Run Simulation']");
        waitVisibilityOfElement("//h2[contains(text(),'"+batchName+" Result')]");
    }

    public boolean dataOnBatchResultsPageMatchToInputData() {
        panelHeight = getText("(//span[text()='Panel Height'])/../span[3]");
        panelOpacity = getText("(//span[text()='Panel Opacity'])/../span[3]");
        panelSize = getText("(//span[text()='Panel Size'])/../span[3]");
        initialOffset = getText("(//span[text()='Initial Offset'])/../span[3]");
        numberOfPanels = getText("(//span[text()='Number of Panels'])/../span[3]");
        spaceBetweenPanels = getText("(//span[text()='Space Between Panels'])/../span[3]");
        fieldSize = getText("(//span[text()='Field Size'])/../span[3]");
        azimuth = getText("(//span[text()='Azimuth'])/../span[3]");
        structureType = getText("(//span[text()='Structure Type'])/../span[3]");
        panelTilt = getText("(//span[text()='Panel Tilt'])/../span[3]");
        panelTranslation = getText("(//span[text()='Panel Translation'])/../span[3]");
        maximalLateralTranslation = getText("(//span[text()='Maximal Lateral Translation'])/../span[3]");
        lateralOffset = getText("(//span[text()='Lateral Offset'])/../span[3]");
        periodName = getText("(//span[text()='Period Name'])/../span[3]");
        startingPoint = getText("(//span[text()='Starting Point'])/../span[3]");
        endingPoint = getText("(//span[text()='Ending Point'])/../span[3]");
        weatherDatasetType = getText("(//span[text()='Dataset type'])/../span[3]");
        weatherSamplingRate = getText("(//span[text()='Sampling rate'])/../span[3]");
        weatherTimezone = getText("(//span[text()='Timezone'])/../span[3]");
        prodDatasetType = getText("(//span[text()='Dataset type'])[2]/../span[3]");
        prodSamplingRate = getText("(//span[text()='Sampling rate'])[2]/../span[3]");
        prodTimezone = getText("(//span[text()='Timezone'])[2]/../span[3]");

        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//button[@class='styled_button__WKqbu']");
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Timezone']");

        return (getText("(//span[text()='Panel Height'])/../span[3]").equals(panelHeight) &&
                getText("(//span[text()='Panel Opacity'])/../span[3]").equals(panelOpacity) &&
                getText("(//span[text()='Panel Size'])/../span[3]").equals(panelSize) &&
                getText("(//span[text()='Initial Offset'])/../span[3]").equals(initialOffset) &&
                getText("(//span[text()='Number of Panels'])/../span[3]").equals(numberOfPanels) &&
                getText("(//span[text()='Space Between Panels'])/../span[3]").equals(spaceBetweenPanels) &&
                getText("(//span[text()='Field Size'])/../span[3]").equals(fieldSize) &&
                getText("(//span[text()='Azimuth'])/../span[3]").equals(azimuth) &&
                getText("(//span[text()='Structure Type'])/../span[3]").equals(structureType) &&
                getText("(//span[text()='Panel Tilt'])/../span[3]").equals(panelTilt) &&
                getText("(//span[text()='Panel Translation'])/../span[3]").equals(panelTranslation) &&
                getText("(//span[text()='Maximal Lateral Translation'])/../span[3]").equals(maximalLateralTranslation) &&
                getText("(//span[text()='Lateral Offset'])/../span[3]").equals(lateralOffset) &&
                getText("(//span[text()='Period Name'])/../span[3]").equals(periodName) &&
                getText("(//span[text()='Starting Point'])/../span[3]").equals(startingPoint) &&
                getText("(//span[text()='Ending Point'])/../span[3]").equals(endingPoint) &&
                getText("(//span[text()='Dataset type'])/../span[3]").equals(weatherDatasetType) &&
                getText("(//span[text()='Sampling rate'])/../span[3]").equals(weatherSamplingRate) &&
                getText("(//span[text()='Timezone'])/../span[3]").equals(weatherTimezone) &&
                getText("(//span[text()='Dataset type'])[2]/../span[3]").equals(prodDatasetType) &&
                getText("(//span[text()='Sampling rate'])[2]/../span[3]").equals(prodSamplingRate) &&
                getText("(//span[text()='Timezone'])[2]/../span[3]").equals(prodTimezone));
    }

    public void downloadToLocalDirectory () throws InterruptedException {
        /// скачиваем результат батча
        waitVisibilityOfElement("//*[text()='Download']");
        click("//*[text()='Download']");
        waitStalenessOfElement("//div[@class='styled_spinner__yFkql spinner-border']");

        /// ждём окончания закачки батча в локальную директорию
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        for (int i=0; i<20; i++) {
            if (folder.exists() && folder.listFiles().length > 0) {
                if (folder.listFiles()[0].toString().
                        substring((folder.listFiles()[0].toString().length() - 3),
                                folder.listFiles()[0].toString().length()).equals("zip"))
                {System.out.println("ZIP found"); i=20; }
            }
            Thread.sleep(500);
        }
    }

    public void unZipLocalFile() {
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        File[] listOfFiles = folder.listFiles();
        String archiveName = listOfFiles[0].getName();
        String zipFilePath = System.getProperty("user.dir") + separator+"download" + separator+ archiveName;
        File destDir = new File("download");

        if (!destDir.exists()) destDir.mkdirs();
        FileInputStream FiS;

        byte[] buffer = new byte[1024];
        try {
            FiS = new FileInputStream(zipFilePath);
            ZipInputStream ZiS = new ZipInputStream(FiS);
            ZipEntry ZE = ZiS.getNextEntry();
            while (ZE != null) {
                String fileName = ZE.getName();
                File newFile = new File(destDir + separator + fileName);

                new File(newFile.getParent()).mkdirs();
                FileOutputStream FoS = new FileOutputStream(newFile);
                int len;
                while ((len = ZiS.read(buffer)) > 0) {
                    FoS.write(buffer, 0, len);
                }
                FoS.close();

                ZiS.closeEntry();
                ZE = ZiS.getNextEntry();
            }

            ZiS.closeEntry();
            ZiS.close();
            FiS.close();
        } catch (IOException e) {
            e.printStackTrace(); }
    }

    public int [] readExcel(String path) throws IOException {
        File file = new File(path);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook wb = new XSSFWorkbook(inputStream);
        XSSFSheet bookmark=wb.getSheet("Analysis");

        XSSFRow row=bookmark.getRow(3);
        XSSFCell cell=row.getCell(2);
        int areaUnderPanels = (int) cell.getNumericCellValue();
        row=bookmark.getRow(3);
        cell=row.getCell(3);
        int areaBetweenPanels = (int) cell.getNumericCellValue();
        row=bookmark.getRow(3);
        cell=row.getCell(4);
        int areaAgrilpv = (int) cell.getNumericCellValue();
        int [] out = {areaUnderPanels, areaBetweenPanels, areaAgrilpv};
        return out;
    }

    public boolean batchResultsMatchToDataFromArchive () throws IOException {
        File folder = new File(System.getProperty("user.dir")+ separator+"download");
        //File[] listOfFiles = folder.listFiles();
        //String archiveName = listOfFiles[0].getName();
        int resultExel [] = readExcel(System.getProperty("user.dir") + separator +"download" +
                        separator + simulationName+" 1" + separator + periodName + separator + "data.xlsx");
        int areaUnderPanels = resultExel[0];
        int areaBetweenPanels = resultExel[1];
        int areaAgrilPV = resultExel[2];
        waitVisibilityOfElement("//tbody//td[2]");
        return (Integer.valueOf(getText("//tbody//td[2]")).equals(areaBetweenPanels) &&
                Integer.valueOf(getText("//tbody//td[3]")).equals(areaUnderPanels) &&
                Integer.valueOf(getText("//tbody//td[4]")).equals(areaAgrilPV));
    }
}