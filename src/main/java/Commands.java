import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Commands {
    WebDriver driver;
    WebDriverWait wait;
    String loginURL = "http://admin:48526@agrisoleo.groupbwt.com/login";
    String clientsURL = "http://agrisoleo.groupbwt.com/clients";

    String createNewButton = "//*[text()='Create new']";
    String createButton = "//*[text()='Create']";
    String addButton = "//*[text()='Add']";
    String editButton = "//a[text()='Edit']";
    String saveButton = "//*[text()='Save']";
    String cancelButton = "//*[text()='Cancel']";
    String leavePageButton = "//*[text()='Leave page']";
    String deleteButton = "//*[text()='Delete']";

    String clearValue = Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE);
    public String replaceValue(String newValue) {
        return Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE) + newValue;
    }

    String nameField = "//input[@name='name']";
    String dataRequired = "//*[text()='This field is required']";
    String longNameError = "//*[text()='Max length are 50 charts']";

    String longName = "123456789012345678901234567890123456789012345678901";
    String clientName = "autoTest";
    String clientEmail = "autoTest@autoTest.com";
    String clientDescription = "autoTest description";
    String projectName = "autoTest Project new";
    String structureName = "Structure";
    String samplePointName = "New Sample Point";
    String cropName = "Crop";
    String periodName = "Period";
    String weatherDatasetName = "weather";
    String productionDatasetName = "prod";
    String simulationName = "Sim";
    String batchName = "Batch";

    public WebElement findElement (String path) {
        return driver.findElement(By.xpath(path));
    }

    public void sendText(String path, String text) {
        driver.findElement(By.xpath(path)).sendKeys(text);
    }

    public void copyPaste(String from, String to) {
        driver.findElement(By.xpath(from)).sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.chord(Keys.CONTROL, "c"));
        driver.findElement(By.xpath(to)).sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.chord(Keys.CONTROL, "v"));
    }

    public void click (String path) {
        driver.findElement(By.xpath(path)).click();
    }

    public void waitPresenceOfElement (String path) {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }

    public void waitVisibilityOfElement (String path) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(path)));
    }

    public void waitStalenessOfElement (String path) {
        wait.until(ExpectedConditions.stalenessOf(findElement(path)));
    }

    public boolean isDisplayed (String path) {
        return driver.findElement(By.xpath(path)).isDisplayed();
    }

    public int findElementsSize (String path) {
        return driver.findElements(By.xpath(path)).size();
    }

    public String getAttributeValue (String path) {
         return driver.findElement(By.xpath(path)).getAttribute("valueAsNumber");
    }

    public String getAttributeText (String path) {
        return driver.findElement(By.xpath(path)).getAttribute("textContent");
    }

    public String getText (String path) {
        return driver.findElement(By.xpath(path)).getText();
    }

}