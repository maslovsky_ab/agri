import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class API {

    public JSONObject getClientsList() throws IOException, JSONException {
        URL url = new URL("http://agrisoleo-api.groupbwt.com:9995/api/v1/clients?order_by=created_at&order=asc");
        HttpURLConnection http = (HttpURLConnection)url.openConnection();
        http.setRequestProperty("Authorization", "Basic "+ "YWRtaW46MTIzMTIz");
        InputStream in = http.getInputStream();
        //String body = IOUtils.toString(in);
        JSONObject body = new JSONObject(IOUtils.toString(in));
        //System.out.println(body);
        return (body);
    }

}
