import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Arrays;
import java.util.List;

public class Structures extends Commands {
    String panelHeight = "2",
            panelOpacity = "1",
            panelSizeX = "5",
            panelSizeY = "1",
            initialOffsetX = "3",
            initialOffsetY = "2",
            numberOfPanelsX = "9",
            numberOfPanelsY = "14",
            spaceBetweenPanelsX = "5.5",
            spaceBetweenPanelsY = "3.5",
            fieldSizeX = "50",
            fieldSizeY = "50",
            resolution = "10",
            azimuth = "0",
            structureType = "Fixed",
            panelTilt = "15",
            maximalLateralTranslation = "3",
            lateralOffset = "0",
            sunPosition = "Fixed",
            sunPositionAzimuth = "20",
            sunPositionZenith = "20";

    public Structures (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void newStructureLoadDefaultValues() {
        click(createNewButton);
    }

    public boolean defaultValuesLoaded() {
        return (getAttributeValue("//input[@name='panel_height']").equals(panelHeight) &&
                getAttributeText("(//label[text()='Panel Opacity']/..//div)[4]").equals(panelOpacity) &&
                getAttributeValue("//input[@name='panel_x']").equals(panelSizeX) &&
                getAttributeValue("//input[@name='panel_y']").equals(panelSizeY) &&
                getAttributeValue("//input[@name='initial_offset_x']").equals(initialOffsetX) &&
                getAttributeValue("//input[@name='initial_offset_y']").equals(initialOffsetY) &&
                getAttributeValue("//input[@name='panels_number_x']").equals(numberOfPanelsX) &&
                getAttributeValue("//input[@name='panels_number_y']").equals(numberOfPanelsY) &&
                getAttributeValue("//input[@name='panels_gap_x']").equals(spaceBetweenPanelsX) &&
                getAttributeValue("//input[@name='panels_gap_y']").equals(spaceBetweenPanelsY) &&
                getAttributeValue("//input[@name='field_size_x']").equals(fieldSizeX) &&
                getAttributeValue("//input[@name='field_size_y']").equals(fieldSizeY) &&
                getAttributeValue("//input[@name='resolution']").equals(resolution) &&
                getAttributeText("(//label[text()='Azimuth']/..//div)[4]").equals(azimuth) &&
                getAttributeText("//h2[text()='Structure Type']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']").equals(structureType) &&
                getAttributeText("(//label[text()='Panel Tilt']/..//div)[4]").equals(panelTilt) &&
                getAttributeValue("//input[@name='translation_max_delta']").equals(maximalLateralTranslation) &&
                getAttributeValue("//input[@name='translation_init_delta']").equals(lateralOffset) &&
                getAttributeText("//h2[text()='Sun position']/..//button[@class='styled_button__eZ6Za styled_checked__wHig7']").equals(sunPosition) &&
                getAttributeText("(//label[text()='Azimuth']/..//div)[8]").equals(sunPositionAzimuth) &&
                getAttributeText("(//label[text()='Zenith']/..//div)[4]").equals(sunPositionZenith));
    }

    public void makeNewStructure(){
        click(createNewButton);
        sendText(nameField,structureName);
        click(saveButton);
        waitPresenceOfElement("//p[text()='"+structureName+"']");
    }

    public boolean newStructureSavedCorrectly() {
        click("//p[text()='"+structureName+"']");
            waitPresenceOfElement("//span[text()='Draw']");
        return defaultValuesLoaded();
    }

    public void clearStructureFields() {
        click("//p[text()='"+structureName+"']");
        waitPresenceOfElement("//span[text()='Draw']");
        List<String> elements = Arrays.asList("name", "panel_height", "panel_x", "panel_y","initial_offset_x",
                "initial_offset_y", "panels_number_x", "panels_number_y", "panels_gap_x", "panels_gap_y",
                "field_size_x", "field_size_y", "resolution", "translation_max_delta", "translation_init_delta");
        for (String element : elements) { sendText("//input[@name='" + element + "']", clearValue); }
        /*sendText("//input[@name='name']", clearValue);
        sendText("//input[@name='panel_height']", clearValue);
        sendText("//input[@name='panel_x']", clearValue);
        sendText("//input[@name='panel_y']", clearValue);
        sendText("//input[@name='initial_offset_x']", clearValue);
        sendText("//input[@name='initial_offset_y']", clearValue);
        sendText("//input[@name='panels_number_x']", clearValue);
        sendText("//input[@name='panels_number_y']", clearValue);
        sendText("//input[@name='panels_gap_x']", clearValue);
        sendText("//input[@name='panels_gap_y']", clearValue);
        sendText("//input[@name='field_size_x']", clearValue);
        sendText("//input[@name='field_size_y']", clearValue);
        sendText("//input[@name='resolution']", clearValue);
        sendText("//input[@name='translation_max_delta']", clearValue);
        sendText("//input[@name='translation_init_delta']", clearValue);*/
        click(saveButton);
    }

    public boolean clearedStructureFieldsErrorMessageDisplayed() {
        List<String> elements = Arrays.asList("name", "panel_height", "panel_x", "panel_y","initial_offset_x",
                "initial_offset_y", "panels_number_x", "panels_number_y", "panels_gap_x", "panels_gap_y",
                "field_size_x", "field_size_y", "resolution", "translation_max_delta", "translation_init_delta");
        boolean result = true;

        waitPresenceOfElement("//span[text()='Draw']");
        for (String element : elements)
            { if (!isDisplayed("//input[@name='" + element + "']/../.."+dataRequired)) result=false; }
        /*boolean result = findElementsSize(dataRequired) == 15;
        boolean result = (isDisplayed("//input[@placeholder='Structure Name']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panel_height']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panel_x']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panel_y']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='initial_offset_x']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='initial_offset_y']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panels_number_x']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panels_number_y']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panels_gap_x']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='panels_gap_y']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='field_size_x']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='field_size_y']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='resolution']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='translation_max_delta']/..//../span[text()='This field is required']") &&
                isDisplayed("//input[@name='translation_init_delta']/..//../span[text()='This field is required']"));*/
        click(cancelButton);
        click(leavePageButton);
        return result;
    }

    public void enterIncorrectVariablesValuesStructure() {
        List<String> elements = Arrays.asList("panel_height", "panel_x", "panel_y",
                "panels_number_x", "panels_number_y", "panels_gap_x", "panels_gap_y",
                "field_size_x", "field_size_y", "resolution", "translation_max_delta", "translation_init_delta");
        List<String> values = Arrays.asList("-1", "0", "0", "-1", "-1", "-1", "-1", "0", "0", "0", "0", "-1");

        click("//p[text()='"+structureName+"']");
        waitPresenceOfElement("//span[text()='Draw']");
        for (String element : elements)
            { sendText("//input[@name='"+element+"']", replaceValue(values.get(elements.indexOf(element)))); }
        /*sendText("//input[@name='panel_height']", replaceValue("-1"));
        sendText("//input[@name='panel_x']", replaceValue("0"));
        sendText("//input[@name='panel_y']", replaceValue("0"));
        sendText("//input[@name='panels_number_x']", replaceValue("-1"));
        sendText("//input[@name='panels_number_y']", replaceValue("-1"));
        sendText("//input[@name='panels_gap_x']", replaceValue("-1"));
        sendText("//input[@name='panels_gap_y']", replaceValue("-1"));
        sendText("//input[@name='field_size_x']", replaceValue("0"));
        sendText("//input[@name='field_size_y']", replaceValue("0"));
        sendText("//input[@name='resolution']", replaceValue("0"));
        sendText("//input[@name='translation_max_delta']", replaceValue("0"));
        sendText("//input[@name='translation_init_delta']", replaceValue("-1"));*/
        click(saveButton);
    }

    public boolean incorrectVariablesValuesErrorMessageDisplay() {
        List<String> moreZeroElements = Arrays.asList("panel_height", "panel_x", "panel_y");
        List<String> zeroElements = Arrays.asList("panels_number_x", "panels_number_y", "panels_gap_x", "panels_gap_y", "translation_init_delta");
        List<String> oneElements = Arrays.asList("field_size_x", "field_size_y", "resolution", "translation_max_delta");
        boolean result = true;

        for (String element : moreZeroElements)
        { if (!isDisplayed("//input[@name='" + element + "']/..//../span[text()='Value must be more then 0']")) result=false; }
        for (String element : zeroElements)
        { if (!isDisplayed("//input[@name='" + element + "']/..//../span[text()='Minimum value is 0']")) result=false; }
        for (String element : oneElements)
        { if (!isDisplayed("//input[@name='" + element + "']/..//../span[text()='Minimum value is 1']")) result=false; }
        /*boolean result = (isDisplayed("//input[@name='panel_height']/..//../span[text()='Value must be more then 0']") &&
                isDisplayed("//input[@name='panel_x']/..//../span[text()='Value must be more then 0']") &&
                isDisplayed("//input[@name='panel_y']/..//../span[text()='Value must be more then 0']") &&
                isDisplayed("//input[@name='panels_number_x']/..//../span[text()='Minimum value is 0']") &&
                isDisplayed("//input[@name='panels_number_y']/..//../span[text()='Minimum value is 0']") &&
                isDisplayed("//input[@name='panels_gap_x']/..//../span[text()='Minimum value is 0']") &&
                isDisplayed("//input[@name='panels_gap_y']/..//../span[text()='Minimum value is 0']") &&
                isDisplayed("//input[@name='field_size_x']/..//../span[text()='Minimum value is 1']") &&
                isDisplayed("//input[@name='field_size_y']/..//../span[text()='Minimum value is 1']") &&
                isDisplayed("//input[@name='resolution']/..//../span[text()='Minimum value is 1']") &&
                isDisplayed("//input[@name='translation_max_delta']/..//../span[text()='Minimum value is 1']") &&
                isDisplayed("//input[@name='translation_init_delta']/..//../span[text()='Minimum value is 0']"));*/
        click(cancelButton);
        click(leavePageButton);
        return result;
    }

    public void incorrectVariablesValuesTypeStructure() {
        List<String> elements = Arrays.asList("panels_number_x", "panels_number_y", "field_size_x", "field_size_y", "resolution");
        click("//p[text()='"+structureName+"']");
        waitPresenceOfElement("//span[text()='Draw']");
        for (String element : elements) {copyPaste("//input[@name='panels_gap_x']", "//input[@name='"+element+"']");}
        /*copyPaste("//input[@name='panels_gap_x']", "//input[@name='panels_number_x']");
        copyPaste("//input[@name='panels_gap_x']", "//input[@name='panels_number_y']");
        copyPaste("//input[@name='panels_gap_x']", "//input[@name='field_size_x']");
        copyPaste("//input[@name='panels_gap_x']", "//input[@name='field_size_y']");
        copyPaste("//input[@name='panels_gap_x']", "//input[@name='resolution']");*/
        click(saveButton);
    }

    public boolean incorrectVariablesValuesTypeErrorMessageDisplay() {
        boolean result = true;
        List<String> elements = Arrays.asList("panels_number_x", "panels_number_y", "field_size_x", "field_size_y","resolution");
            for (String element : elements) {
                if (!isDisplayed("//input[@name='" + element + "']/..//../span[text()='Value must be integer']"))
                    result = false;
                }
        /*boolean result = (
                isDisplayed("//input[@name='panels_number_x']/..//../span[text()='Value must be integer']") &&
                isDisplayed("//input[@name='panels_number_y']/..//../span[text()='Value must be integer']") &&
                isDisplayed("//input[@name='field_size_x']/..//../span[text()='Value must be integer']") &&
                isDisplayed("//input[@name='field_size_y']/..//../span[text()='Value must be integer']") &&
                isDisplayed("//input[@name='resolution']/..//../span[text()='Value must be integer']"));*/
        click(cancelButton);
        click(leavePageButton);
        return result;
    }

    public void addSamplePointsToNewStructure() {
        click("//p[text()='"+structureName+"']");
        waitPresenceOfElement("//span[text()='Add new point']");
        click("//*[text()='Add new point']");
        sendText("//label[text()='Point name']/.."+nameField, samplePointName);
        sendText("//input[@name='x']", "10");
        sendText("//input[@name='y']", "20");
        click(addButton);
        waitPresenceOfElement("//td[text()='"+samplePointName+"']");
    }

    public boolean samplePointsInNewStructureSavedCorrectly() {
        boolean result = (getText("(//table[@class='styled_table__7-WLY']//td)[1]").equals(samplePointName) &&
                getText("(//table[@class='styled_table__7-WLY']//td)[2]").equals("10, 20"));
        click(cancelButton);
        click(leavePageButton);
        return result;
    }

}