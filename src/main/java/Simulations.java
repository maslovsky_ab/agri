import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Simulations extends Commands {

    String panelHeight, panelOpacity, panelSize, initialOffset, numberOfPanels, spaceBetweenPanels, fieldSize,azimuth,
            structureType, panelTilt, panelMaxTilt, panelTranslation, maximalLateralTranslation, lateralOffset;
    String periodName, startingPoint, endingPoint;
    String weatherDatasetType, weatherSamplingRate, weatherTimezone;
    String prodDatasetType, prodSamplingRate, prodTimezone;

    public Simulations (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void makeNewSimulation() {
        click("//a[text()='Simulations']");
        waitVisibilityOfElement(createNewButton);
        click(createNewButton);
        waitVisibilityOfElement("//*[text()='Electrical Production Dataset']/..//*[text()='Select']");
        sendText(nameField, simulationName);
        sendText("//span[text()='Structures']/..//input", structureName+Keys.RETURN);
        waitVisibilityOfElement("//*[text()='Lateral Offset']");
        sendText("//span[text()='Crops']/..//input", cropName+Keys.RETURN);
        waitVisibilityOfElement("//*[text()='Ending Point']");
        sendText("//span[text()='Weather Dataset']/..//input", weatherDatasetName+Keys.RETURN);
        waitVisibilityOfElement("//*[text()='Dataset type']/..//*[text()='weather']");
        driver.findElement(By.xpath("//span[text()='Electrical Production Dataset']/..//input")).sendKeys("prod", Keys.RETURN);
        sendText("//label[text()='Simulation Frequency']/..//input", "1 hour"+Keys.RETURN);
        waitVisibilityOfElement("//*[text()='1 hour']");
        sendText("//textarea[@name='description']", "description");
        waitVisibilityOfElement("//textarea[text()='description']");
        click(saveButton);
        waitStalenessOfElement("//div[@class='styled_spinner__yFkql spinner-border']");
        waitVisibilityOfElement("//input[@value='"+simulationName+"']");
        click(cancelButton);
        waitVisibilityOfElement("//p[text()='"+simulationName+"']");
    }

    public void readStructureData(){
        click("//a[text()='Structures']");
        waitVisibilityOfElement("//h2[text()='Structures']/..//..//p[text()='"+structureName+"']");
        click("//button[@class='styled_button__e6WEB']");
        waitPresenceOfElement("//span[text()='Draw']");
        panelHeight = getAttributeValue("//input[@name='panel_height']")+" m";
        panelOpacity = getText("//label[text()='Panel Opacity']/..//div[@class='styled_tooltip__i6Njk']");
        panelSize = getAttributeValue("//input[@name='panel_x']") +
                "x"+ getAttributeValue("//input[@name='panel_y']") + " m";
        initialOffset = getAttributeValue("//input[@name='initial_offset_x']") +
                "x"+ getAttributeValue("//input[@name='initial_offset_y']") + " m";
        numberOfPanels = getAttributeValue("//input[@name='panels_number_x']") +
                "x"+ getAttributeValue("//input[@name='panels_number_y']");
        spaceBetweenPanels = getAttributeValue("//input[@name='panels_gap_x']") +
                "x"+ getAttributeValue("//input[@name='panels_gap_y']");
        fieldSize = getAttributeValue("//input[@name='field_size_x']") +
                "x"+ getAttributeValue("//input[@name='field_size_y']") + " m";
        azimuth = getText("//label[text()='Azimuth']/..//div[@class='styled_tooltip__i6Njk']") + " º";
        structureType = getText("//button[@class='styled_button__eZ6Za styled_checked__wHig7']");
        if (structureType.equalsIgnoreCase("Fixed")) {
            panelTilt = getText("//label[text()='Panel Tilt']/..//div[@class='styled_tooltip__i6Njk']");
        }
        else {
            panelMaxTilt = getText("//label[text()='Panel max Tilt']/..//div[@class='styled_tooltip__i6Njk']");
        }
        if (driver.findElement(By.xpath("//label[text()='Panel Translation']//span[@class='styled_checkmark__sdpA6']")).
                getCssValue("background-color").equals("rgba(86, 161, 121, 1)"))
            panelTranslation = "true";
        else panelTranslation = "false";
        maximalLateralTranslation = getAttributeValue("//input[@name='translation_max_delta']") + " m";
        lateralOffset = getAttributeValue("//input[@name='translation_init_delta']") + " m";
    }

    public void readCropData(){
        click("//button[@type='button']/../a[text()='Crops']");
        waitVisibilityOfElement("//p[text()='"+cropName+"']/../..//button[@type='button']");
        click("//p[text()='"+cropName+"']/../..//button[@type='button']");
        waitVisibilityOfElement("//tbody//td");
        periodName = getText("//tbody//td");
        startingPoint = LocalDate.parse(getText("(//tbody//td)[3]")).format(DateTimeFormatter.ofPattern("dd MMM", Locale.US));
        endingPoint = LocalDate.parse(getText("(//tbody//td)[4]")).format(DateTimeFormatter.ofPattern("dd MMM", Locale.US));
    }

    public void readWeatherDatasetData(){
        click("//button[@type='button']/../a[text()='Datasets']");
        waitVisibilityOfElement("//p[@class='DatasetsCard_name__yfEN5']");
        click("//p[@class='DatasetsCard_name__yfEN5']");
        waitVisibilityOfElement("//p[text()='Datasets']");
        weatherDatasetType = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
        weatherSamplingRate = getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']").substring(0, 1)+"H";
        weatherTimezone = getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
    }

    public void readProductionDatasetData(){
        click("//button[@type='button']/../a[text()='Datasets']");
        waitVisibilityOfElement("//p[@class='DatasetsCard_name__yfEN5']");
        click("(//p[@class='DatasetsCard_name__yfEN5'])[2]");
        waitVisibilityOfElement("//div[text()='Production']");

        prodDatasetType = getText("//label[text()='Dataset type']/..//div[@class='select__single-value css-1dimb5e-singleValue']");
        prodSamplingRate = getText("//label[text()='Sampling rate']/..//div[@class='select__single-value css-1dimb5e-singleValue']").substring(0, 1)+"H";
        prodTimezone = getText("//label[text()='Timezone']/..//div[@class='select__single-value css-1dimb5e-singleValue']");

        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//h2[text()='Simulations']");
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("(//span[text()='Panel Height']/..//span[@class='styled_text__SNezk'])[2]");
    }

    public boolean newSimulationCreatedCorrectly()  {
    boolean result = (getText("(//span[text()='Panel Height']/..//span[@class='styled_text__SNezk'])[2]").equals(panelHeight) &&
                getText("(//span[text()='Panel Opacity']/..//span[@class='styled_text__SNezk'])[2]").equals(panelOpacity) &&
                getText("(//span[text()='Panel Size']/..//span[@class='styled_text__SNezk'])[2]").equals(panelSize) &&
                getText("(//span[text()='Initial Offset']/..//span[@class='styled_text__SNezk'])[2]").equals(initialOffset) &&
                getText("(//span[text()='Number of Panels']/..//span[@class='styled_text__SNezk'])[2]").equals(numberOfPanels) &&
                getText("(//span[text()='Space Between Panels']/..//span[@class='styled_text__SNezk'])[2]").equals(spaceBetweenPanels) &&
                getText("(//span[text()='Field Size']/..//span[@class='styled_text__SNezk'])[2]").equals(fieldSize) &&
                getText("(//span[text()='Azimuth']/..//span[@class='styled_text__SNezk'])[2]").equals(azimuth) &&
                getText("(//span[text()='Panel Translation']/..//span[@class='styled_text__SNezk'])[2]").equalsIgnoreCase(panelTranslation) &&
                getText("(//span[text()='Maximal Lateral Translation']/..//span[@class='styled_text__SNezk'])[2]").equals(maximalLateralTranslation) &&
                getText("(//span[text()='Lateral Offset']/..//span[@class='styled_text__SNezk'])[2]").equals(lateralOffset) &&
                getText("(//span[text()='Period Name']/..//span[@class='styled_text__SNezk'])[2]").equals(periodName) &&
                getText("(//span[text()='Starting Point']/..//span[@class='styled_text__SNezk'])[2]").equals(startingPoint) &&
                getText("(//span[text()='Ending Point']/..//span[@class='styled_text__SNezk'])[2]").equals(endingPoint) &&
                getText("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[2]").equalsIgnoreCase(weatherDatasetType) &&
                getText("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[2]").equals(weatherSamplingRate) &&
                getText("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[2]").equals(weatherTimezone) &&
                getText("(//span[text()='Dataset type']/..//span[@class='styled_text__SNezk'])[4]").equalsIgnoreCase(prodDatasetType) &&
                getText("(//span[text()='Sampling rate']/..//span[@class='styled_text__SNezk'])[4]").equals(prodSamplingRate) &&
                getText("(//span[text()='Timezone']/..//span[@class='styled_text__SNezk'])[4]").equals(prodTimezone)) &&
                getText("(//span[text()='Structure Type']/..//span[@class='styled_text__SNezk'])[2]").equalsIgnoreCase(structureType);

        if (result){
            if (structureType.equalsIgnoreCase("Fixed")) {
                result = getText("(//span[text()='Panel Tilt']/..//span[@class='styled_text__SNezk'])[2]").equals(panelTilt);
            }
            else result = getText("(//span[text()='Panel max Tilt']/..//span[@class='styled_text__SNezk'])[2]").equals(panelMaxTilt);
        }
        return  result;

    }

    public void editSimulation() {
        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//p[text()='"+simulationName+"']");
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Run Simulation']");
        sendText("//input[@name='resolution']", replaceValue("5"));
        sendText("//textarea[@name='description']", "2");
        sendText("//label[text()='Simulation Frequency']/..//input[@class='select__input']", "2 hour"+Keys.RETURN);
        sendText("//input[@name='max_scale_value']", replaceValue("5000"));
        sendText("//input[@name='name']", "2");
        click(saveButton);
        waitVisibilityOfElement("//p[text()='"+simulationName+"2']");
    }

    public boolean editedSimulationSavedCorrectly() {
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Timezone']");
        return(getAttributeValue("//input[@name='resolution']").equals("5") &&
                getText("//textarea[@name='description']").equals("description2") &&
                getText("//label[text()='Simulation Frequency']/..//div[@class='select__single-value css-1dimb5e-singleValue']").equals("2 hour") &&
                getAttributeValue("//input[@name='max_scale_value']").equals("5000"));
    }

    public void duplicateSimulation() {
        click("(//button[@class='styled_button__WKqbu'])[2]");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
    }

    public boolean simulationDuplicatedCorrectly() {
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Timezone']");

        panelHeight = getText("//span[text()='Panel Height']/../span[3]");
        panelOpacity = getText("//span[text()='Panel Opacity']/../span[3]");
        panelSize = getText("//span[text()='Panel Size']/../span[3]");
        initialOffset = getText("//span[text()='Initial Offset']/../span[3]");
        numberOfPanels = getText("//span[text()='Number of Panels']/../span[3]");
        spaceBetweenPanels = getText("//span[text()='Space Between Panels']/../span[3]");
        fieldSize = getText("//span[text()='Field Size']/../span[3]");
        azimuth = getText("//span[text()='Azimuth']/../span[3]");
        structureType = getText("//span[text()='Structure Type']/../span[3]");
        panelTilt = getText("//span[text()='Panel Tilt']/../span[3]");
        panelTranslation = getText("//span[text()='Panel Translation']/../span[3]");
        maximalLateralTranslation = getText("//span[text()='Maximal Lateral Translation']/../span[3]");
        lateralOffset = getText("//span[text()='Lateral Offset']/../span[3]");

        periodName = getText("//span[text()='Period Name']/../span[3]");
        startingPoint = getText("//span[text()='Starting Point']/../span[3]");
        endingPoint = getText("//span[text()='Ending Point']/../span[3]");

        weatherDatasetType = getText("//span[text()='Dataset type']/../span[3]");
        weatherSamplingRate = getText("//span[text()='Sampling rate']/../span[3]");
        weatherTimezone = getText("//span[text()='Timezone']/../span[3]");

        prodDatasetType = getText("(//span[text()='Dataset type']/../span[3])[2]");
        prodSamplingRate = getText("(//span[text()='Sampling rate']/../span[3])[2]");
        prodTimezone = getText("(//span[text()='Timezone']/../span[3])[2]");

        click("//a[text()='Simulations']");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
        click("(//button[@class='styled_button__WKqbu'])[3]");
        waitVisibilityOfElement("//span[text()='Timezone']");

        return(getText("//span[text()='Panel Height']/../span[3]").equals(panelHeight) &&
                getText("//span[text()='Panel Opacity']/../span[3]").equals(panelOpacity) &&
                getText("//span[text()='Panel Size']/../span[3]").equals(panelSize) &&
                getText("//span[text()='Initial Offset']/../span[3]").equals(initialOffset) &&
                getText("//span[text()='Number of Panels']/../span[3]").equals(numberOfPanels) &&
                getText("//span[text()='Space Between Panels']/../span[3]").equals(spaceBetweenPanels) &&
                getText("//span[text()='Field Size']/../span[3]").equals(fieldSize) &&
                getText("//span[text()='Azimuth']/../span[3]").equals(azimuth) &&
                getText("//span[text()='Structure Type']/../span[3]").equals(structureType) &&
                getText("//span[text()='Panel Tilt']/../span[3]").equals(panelTilt) &&
                getText("//span[text()='Panel Translation']/../span[3]").equals(panelTranslation) &&
                getText("//span[text()='Maximal Lateral Translation']/../span[3]").equals(maximalLateralTranslation) &&
                getText("//span[text()='Lateral Offset']/../span[3]").equals(lateralOffset) &&
                getText("//span[text()='Period Name']/../span[3]").equals(periodName) &&
                getText("//span[text()='Starting Point']/../span[3]").equals(startingPoint) &&
                getText("//span[text()='Ending Point']/../span[3]").equals(endingPoint) &&
                getText("//span[text()='Dataset type']/../span[3]").equals(weatherDatasetType) &&
                getText("//span[text()='Sampling rate']/../span[3]").equals(weatherSamplingRate) &&
                getText("//span[text()='Timezone']/../span[3]").equals(weatherTimezone) &&
                getText("(//span[text()='Dataset type']/../span[3])[2]").equals(prodDatasetType) &&
                getText("(//span[text()='Sampling rate']/../span[3])[2]").equals(prodSamplingRate) &&
                getText("(//span[text()='Timezone']/../span[3])[2]").equals(prodTimezone));
    }

    public void deleteSimulation() {
        click("//button[@class='styled_button__WKqbu styled_red__um3nH']");
        waitPresenceOfElement("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']");
        click("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']");
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 2));
    }

    public boolean simulationDeleted(){
        return (findElementsSize("//div[@class='styled_container__AE6Nl']") < 2);

    }

    public void deleteSimulationFromEdit() {
        click("//button[@class='styled_button__WKqbu']");
        waitVisibilityOfElement("//span[text()='Timezone']");
        click(deleteButton);
        waitPresenceOfElement("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']");
        click("//button[@class='styled_button__MiVhp styled_text__BX30s LeaveModalWindow_button-delete__IKO7V']");
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//div[@class='styled_container__AE6Nl']"), 1));
    }

    public void runSimulation() {
        click("//a[text()='Simulations']");
        waitVisibilityOfElement("//p[text()='" + simulationName + "']");
        click("//p[text()='" + simulationName + "']");
        waitVisibilityOfElement("//span[text()='Electrical Production Dataset']/../..//span[text()='Timezone']");
        waitVisibilityOfElement("//span[text()='Run Simulation']");
        driver.findElement(By.xpath("//input[@name='max_scale_value']")).sendKeys(Keys.PAGE_DOWN);
        waitVisibilityOfElement("//button//span[text()='Run Simulation']");
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Run Simulation']")));
        click("//span[text()='Run Simulation']");
        waitVisibilityOfElement("//*[text()='Enqueued main']");
        driver.findElement(By.xpath("//div[text()='autoTest Project new']/..//input")).sendKeys(Keys.PAGE_UP);
    }

}