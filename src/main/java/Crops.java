import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Crops extends Commands {

    public Crops (WebDriver driver, WebDriverWait Wait) {
        this.driver = driver;
        this.wait = Wait;
    }

    public void makeNewCrop() {
        click("//a[text()='Crops']");
        waitVisibilityOfElement(createNewButton);
        click(createNewButton);
        sendText("//input[@placeholder='Crop Name']", cropName);
        click("//span[text()='Add new Period']");
        waitVisibilityOfElement("//h1[text()='Add new period']");
        sendText("//label[text()='Period Name']/.."+nameField, periodName);
        //driver.findElement(By.xpath("//input[@name='crop_height']")).sendKeys("10"); // "Crop Height" покашо не работает
        sendText("(//input[@placeholder='Date'])[1]", "2022-01-01");
        sendText("(//input[@placeholder='Date'])[2]", "2022-01-02");
        click(addButton);
        waitVisibilityOfElement("//td[text()='"+periodName+"']");
        click(saveButton);
        waitVisibilityOfElement("//p[text()='"+cropName+"']");
    }

    public boolean newCropSavedCorrectly() {
        click("//p[text()='"+cropName+"']");
        waitVisibilityOfElement("(//td)[1]");
        return (getText("(//td)[1]").equals(periodName) &&
                // driver.findElement(By.xpath("(//td)[2]")).getText().equals("10") && // "Crop Height" покашо не работает
                getText("(//td)[3]").equals("2022-01-01") &&
                getText("(//td)[4]").equals("2022-01-02"));
    }

    public boolean newCropErrorMessagesDisplayed() {
        click("//a[text()='Crops']");
        waitVisibilityOfElement(createNewButton);
        click(createNewButton);
        click(saveButton);
        boolean result = false;
        if (isDisplayed(dataRequired) &&
                isDisplayed("//*[text()='There must be at least one period.']") == true) {
            sendText("//input[@placeholder='Crop Name']", longName);
            if (isDisplayed("//*[text()='Max length are 50 charts']") == true) {
                click("//span[text()='Add new Period']");
                waitVisibilityOfElement("//h1[text()='Add new period']");
                click("//*[text()='Add']");
                if (result = findElementsSize(dataRequired) == 3){
                    sendText("//label[text()='Period Name']/..//input[@name='name']", longName);
                    sendText("(//input[@placeholder='Date'])[1]", "2022-01-03");
                    sendText("(//input[@placeholder='Date'])[2]", "2022-01-02");
                    click("//*[text()='Add']");
                    result = (findElementsSize("//*[text()='Max length are 50 charts']") == 2 &&
                    findElementsSize("//*[text()='Invalid date range']") == 2);
                }
            }
        };
        click("//*[text()='Add new period']/../.."+cancelButton);
        click(cancelButton);
        click(leavePageButton);
        return (result);
    }

    public void editCrop() {
        click("//p[text()='"+cropName+"']");
        waitVisibilityOfElement("(//td)[1]");
        sendText("//input[@placeholder='Crop Name']", replaceValue("Crop2"));
        click("//td//button");
        waitVisibilityOfElement("//h1[text()='Edit period']");
        sendText("//label[text()='Period Name']/.."+nameField , replaceValue("Period2"));
        sendText("(//input[@placeholder='Date'])[1]", replaceValue("2022-01-02"));
        sendText("(//input[@placeholder='Date'])[2]", replaceValue("2022-01-03"));
        click(addButton);
        waitVisibilityOfElement("(//td)[1]");
        click(saveButton);
        waitVisibilityOfElement("//p[text()='Crop2']");
    }

    public boolean editedCropSavedCorrectly() {
        boolean result = isDisplayed("//p[text()='Crop2']");
        if (result == true) {
            click("//p[text()='Crop2']");
            waitPresenceOfElement("//button[@class='CropsEdit_button__l43Ys']");
        }
        return (result &&
                getText("(//td)[1]").equals("Period2") &&
                // driver.findElement(By.xpath("(//td)[2]")).getText().equals("20") && // "Crop Height" покашо не работает
                getText("(//td)[3]").equals("2022-01-02") &&
                getText("(//td)[4]").equals("2022-01-03"));
    }

    public boolean editCropErrorMessagesDisplayed() {
        click("//p[text()='"+cropName+"']");
        waitVisibilityOfElement("(//td)[1]");
        click("//td//button"); // edit period
        waitVisibilityOfElement("//h1[text()='Edit period']");
        sendText("//div[@class='modal-content']//input[@name='name']", clearValue);
        sendText("//input[@name='dateStart']", clearValue);
        sendText("//input[@name='dateEnding']", clearValue);
        click(addButton);
        boolean result = false;
        if  (findElementsSize(dataRequired) == 3) {
            sendText("//*[text()='Period Name']/.."+nameField, longName);
            sendText("(//input[@placeholder='Date'])[1]", "2022-01-03");
            sendText("(//input[@placeholder='Date'])[2]", "2022-01-02");
                if (isDisplayed("//*[text()='Max length are 50 charts']") &&
                        findElementsSize("//*[text()='Invalid date range']") == 2) {
                    click("//*[text()='Edit period']/../.."+cancelButton);
                    waitPresenceOfElement("(//tbody//button)[2]");
                    click("(//tbody//button)[2]");
                    sendText(nameField, clearValue);
                    click(saveButton);
                    if (isDisplayed(dataRequired) &&
                            isDisplayed("//span[text()='There must be at least one period.']")){
                        sendText(nameField,longName);
                        if (isDisplayed("//span[text()='Max length are 50 charts']"))
                        {result = true;}
                    }
                }
        }
        click(cancelButton);
        click(leavePageButton);
        return (result);
    }

    public boolean duplicatedCropSameValuesVariables() {
        String cropName = driver.findElement(By.xpath("//p[@class='styled_name__FmpdA']")).getText();
        click("(//button[@class='styled_button__3u9ty'])[2]");
        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        boolean result = false;
        if (getText("(//p[@class='styled_name__FmpdA'])[2]").equals(cropName)) {
            click("(//button[@class='styled_button__3u9ty'])[1]");
            wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//td"), 1));
            String periodName = getText("(//td)[1]");
            String cropHeight = getText("(//td)[2]");
            String startingPoint = getText("(//td)[3]");
            String endingPoint = getText("(//td)[4]");
            click("//span[text()='Cancel']");
            wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
            click("(//button[@class='styled_button__3u9ty'])[3]");
            wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//td"), 1));
            if (getText("(//td)[1]").equals(periodName) &&
                getText("(//td)[2]").equals(cropHeight) &&
                getText("(//td)[3]").equals(startingPoint) &&
                getText("(//td)[4]").equals(endingPoint) == true) { result = true;}
            driver.findElement(By.xpath("//span[text()='Cancel']")).click();
            wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        }
        return result;
    }

    public boolean cropDeleted() {
        click("//button[@class='styled_button__3u9ty styled_red__zocII']");
        click("//*[text()='Delete']");
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//p[@class='styled_name__FmpdA']"), 1));
        return (findElementsSize("//div[@class='styled_container__vhG6v']") == 0);
    }

    public boolean periodDeleted() {
        click("//main//*[text()='"+cropName+"']");
        click("//span[text()='Add new Period']");
        waitVisibilityOfElement("//h1[text()='Add new period']");
        sendText("//label[text()='Period Name']/..//input[@name='name']", "Period1");
        sendText("//input[@name='crop_height']", "10");
        sendText("(//input[@placeholder='Date'])[1]", "2022-01-01");
        sendText("(//input[@placeholder='Date'])[2]", "2022-01-02");
        click(addButton);
        waitVisibilityOfElement("//td[text()='Period1']");
        click("(//td[text()='Period1']/..//button)[2]");
        wait.until(ExpectedConditions.numberOfElementsToBeLessThan(By.xpath("//tr"), 3));
        boolean result = (findElementsSize("//tr") == 2);
        click(cancelButton);
        return result;
    }

}