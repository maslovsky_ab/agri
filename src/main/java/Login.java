import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;


public class Login extends Commands {

    String usernameField = "//input[@name='username']";
    String passwordField = "//input[@name='password']";
    String submitButton = "//button[@type='submit']";
    String homePageRedirect = "//h1[text()='Clients']";
    String invalidCredentials = "//*[contains(text(),'Incorrect login or password')]";
    String shortPassword = "//*[contains(text(),'Min length is 6 charts')]";

    public Login (WebDriver driver, WebDriverWait Wait) {
        this.driver=driver;
        this.wait = Wait;
    }

    public void loginPage(WebDriver driver) {
        driver.get(loginURL);
    }

    public void enterUsernameAndPassword(String username, String password) {
        sendText(usernameField, Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE) + username);
        sendText(passwordField, Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE) + password);
        click(submitButton);
////// удалить после фикса /////////////
        sendText(usernameField, Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE) + username);
        sendText(passwordField, Keys.chord(Keys.CONTROL, "a", Keys.BACK_SPACE) + password);
        click(submitButton);
/////////////////////////////////////////
    }

    public boolean checkCorrectLogin() {
        waitPresenceOfElement(homePageRedirect);
        return isDisplayed(homePageRedirect);
    }

    public boolean incorrectPasswordMessageDisplayed() {
        return isDisplayed(invalidCredentials);
    }

    public boolean incorrectUsernameMessageDisplayed() {
        return isDisplayed(invalidCredentials);
    }

    public boolean blankUsernameFieldMessageDisplayed() {
        return isDisplayed(dataRequired);
    }

    public boolean blankPasswordFieldMessageDisplayed() {
        return isDisplayed(dataRequired);
    }

    public boolean shortPasswordErrorMessageDisplayed() {
        return isDisplayed(shortPassword);
    }

}