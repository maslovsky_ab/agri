import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Tag("Batch")
public class BatchPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Structures structures;
    Crops crops;
    Datasets datasets;
    Simulations simulations;
    Batch batch;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        structures = new Structures(driver, wait);
        crops = new Crops(driver, wait);
        datasets = new Datasets(driver, wait);
        simulations = new Simulations(driver, wait);
        batch = new Batch(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        /*clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();*/
        driver.get("http://agrisoleo.groupbwt.com/clients/703/projects/594/structures");
    }

    @AfterEach
    public void afterTest() {
        //clients.deleteClient();
        //driver.quit();
    }

    @Epic(value = "Batch-page")
    @Feature(value = "make new Batch")
    @Step("в новом Batch отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void makeNewBatch() {
        batch.makeNewBatch();
        Assert.assertTrue(batch.newBatchCreatedCorrectly());
    }

    @Epic(value = "Batch-page")
    @Feature(value = "edit Batch")
    @Step("в отредактированном Batch сохраняются и 'подсвечиваются' значения отличные от 'дефолтных'")
    @Test
    public void editBatch() {
        batch.makeNewBatch();
        batch.editBatch();
        Assert.assertTrue(batch.editedBatchSaved());
    }

    @Epic(value = "Batch-page")
    @Feature(value = "duplicate Batch")
    @Step("в копии Batchа такие же значения как в оригинальном Batchе")
    @Test
    public void duplicateBatch() {
        batch.makeNewBatch();
        batch.duplicateBatch();
        Assert.assertTrue(batch.batchDuplicatedCorrectly());
    }

    @Epic(value = "Batch-page")
    @Feature(value = "delete Batch")
    @Step("удаляем копию Batchа на странице Batchей")
    @Test
    public void deleteBatch() {
        batch.makeNewBatch();
        batch.duplicateBatch();
        batch.deleteBatch();
        Assert.assertTrue(batch.batchDeleted());
    }

    @Epic(value = "Batch-page")
    @Feature(value = "delete Batch")
    @Step("удаляем Batch на странице редактирования Batchа")
    @Test
    public void deleteEditBatch() {
        batch.makeNewBatch();
        batch.duplicateBatch();
        batch.deleteBatchFromEdit();
        Assert.assertTrue(batch.batchDeleted());
    }

    @Epic(value = "Batch-page")
    @Feature(value = "run Batch")
    @Step("данные на странице результат Batch совпадают с входными данными")
    @Test
    public void runBatch() {
        batch.makeNewBatch();
        batch.runBatch();
        Assert.assertTrue(batch.dataOnBatchResultsPageMatchToInputData());
    }

}


