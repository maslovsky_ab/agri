import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;


@Tag("Clients")
public class ClientsPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    API api;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        api = new API();
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
    }

    @AfterEach
    public void afterTest() {
        driver.quit();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Test
    public void makeNewClient() {
        clients.makeNewClient();
        Assert.assertTrue(clients.newClientCreated());
        clients.deleteClient();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("пустое поле 'Имя' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientWithBlankNameField() {
        clients.makeNewClientWithBlankNameField();
        Assert.assertTrue(clients.errorMessageBlankNameFieldDisplayed());
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("'Имя' клиента более 50 символов - отображение сообщения об ошибке")
    @Test
    public void makeNewClientWithLongName() {
        clients.makeNewClientWithLongName();
        Assert.assertTrue(clients.errorMessageVeryLongNameFieldDisplayed());
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("пустое поле 'Емаил' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientWithBlankEmailField() {
        clients.makeNewClientWithBlankEmailField();
        Assert.assertTrue(clients.errorMessageBlankEmailFieldDisplayed());
    }

    @Epic(value = "Clients-page")
    @Feature(value = "make new Client")
    @Step("некорректное написание 'Емаила' клиента - отображение сообщения об ошибке")
    @Test
    public void makeNewClientWithMisspelledEmail() {
        clients.makeNewClientWithMisspelledEmail();
        Assert.assertTrue(clients.errorMessageMisspelledEmailFieldDisplayed());
    }

    @Epic(value = "Clients-page")
    @Feature(value = "edit Client")
    @Step("проверка сохранения данных клиента после редактирования данных")
    @Test
    public void editClient() {
        clients.makeNewClient();
        clients.editClient();
        Assert.assertTrue(clients.editedDataSaved());
        clients.deleteClient();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "delete Client")
    @Test
    public void deleteClient() {
        clients.makeNewClient();
        clients.deleteClient();
        Assert.assertTrue(clients.clientDeleted());
    }

    @Epic(value = "Clients-page")
    @Feature(value = "sorting Clients")
    @Step("сортировка клиентов по возрастанию имён")
    @Test
    public void sortingClientsNameAscendingOrder() {
        clients.makeMultipleClients();
        clients.sortingNameAscending();
        Assert.assertTrue(clients.clientsNameSortedAscending());
        clients.deleteMultipleClients();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "sorting Clients")
    @Step("сортировка клиентов по убыванию имён")
    @Test
    public void sortingClientsNameDescendingOrder() {
        clients.makeMultipleClients();
        clients.sortingNameDescending();
        Assert.assertTrue(clients.clientsNameSortedDescending());
        clients.deleteMultipleClients();
    }
/*///////////////////////////////////// по дате сортировка работает неправильно ////////////////////
    @Epic(value = "Clients-page")
    @Feature(value = "sorting Clients")
    @Step("сортировка клиентов по возрастанию даты изменений")
    @Test
    public void sortingClientsDateAscendingOrder() throws JSONException, IOException {
        //clients.makeMultipleClients();
        clients.sortingDateAscending();
        Assert.assertTrue(clients.clientsDateSortedAscending());
        //clients.deleteMultipleClients();
    }
///////////////////////////////////// по дате сортировка работает неправильно ////////////////////

    @Epic(value = "Clients-page")
    @Feature(value = "sorting Clients")
    @Step("сортировка клиентов по убыванию даты изменений")
    @Test
    public void sortingClientsDateDescendingOrder() {
        clients.makeMultipleClients();
        //clients.sortingDateDescending();
        //Assert.assertTrue(clients.clientsDateSortedDescending());
        clients.deleteMultipleClients();
    }
/////////////////////////////////////////////////////////////////////////////////////
*/
    @Epic(value = "Clients-page")
    @Feature(value = "displaying number of projects")
    @Test
    public void displayingNumberOfProjects() {
        clients.makeNewClient();
        projects.makeNewProject();
        Assert.assertTrue(clients.numberOfProjectsDisplayed());
        clients.deleteClient();
    }

    @Epic(value = "Clients-page")
    @Feature(value = "user name icon display")
    @Test
    public void userNameIconDisplay() {
        clients.makeNewClient();
        Assert.assertTrue(clients.userNameIconDisplayed());
        clients.deleteClient();
    }

}
