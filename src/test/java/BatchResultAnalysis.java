import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static java.io.File.separator;

@Tag("Analysis")
public class BatchResultAnalysis {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Structures structures;
    Crops crops;
    Datasets datasets;
    Simulations simulations;
    Batch batch;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory", System.getProperty("user.dir")+ separator+"download");
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        System.setProperty("download.default_directory", System.getProperty("user.dir")+ separator+"path"+ separator+"to"+ separator+"download");
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        structures = new Structures(driver, wait);
        crops = new Crops(driver, wait);
        datasets = new Datasets(driver, wait);
        simulations = new Simulations(driver, wait);
        batch = new Batch(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        /*clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        batch.makeNewBatch();
        batch.runBatch();*/
        driver.get("http://agrisoleo.groupbwt.com/clients/703/projects/594/batches/295/results/155/simulations/510");
    }

    @AfterEach
    public void afterTest() {
        //clients.deleteClient();
        //driver.quit();
    }

    @Epic(value = "Comparing Batch results on page with results in downloaded Excel")
    @Test
    public void batchResultAnalysis() throws Throwable {
        batch.downloadToLocalDirectory();
        batch.unZipLocalFile();
        Assert.assertTrue(batch.batchResultsMatchToDataFromArchive());
        FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+ separator+"download"));
    }

}


