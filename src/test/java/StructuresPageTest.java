import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Tag("Structures")
public class StructuresPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Structures structures;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        structures = new Structures(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient();
        driver.quit();
    }

    @Epic(value = "Structures-page")
    @Feature(value = "make new Structure")
    @Step("в новую структуру подгружаются корректные дефолтные значения")
    @Test
    public void newStructureLoadDefaultValues() {
        structures.newStructureLoadDefaultValues();
        Assert.assertTrue(structures.defaultValuesLoaded());
    }

    @Epic(value = "Structures-page")
    @Feature(value = "make new Structure")
    @Step("в структура сохраняется корректные значения")
    @Test
    public void makeNewStructure() {
        structures.makeNewStructure();
        Assert.assertTrue(structures.newStructureSavedCorrectly());
    }


    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("для пустых полей отображаются сообщения об ошибке")
    @Test
    public void blankFieldsStructure() {
        structures.makeNewStructure();
        structures.clearStructureFields();
        Assert.assertTrue(structures.clearedStructureFieldsErrorMessageDisplayed());
    }

    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("для недопустимых значений переменных отображаются сообщения об ошибке")
    @Test
    public void incorrectVariablesValuesStructure() {
        structures.makeNewStructure();
        structures.enterIncorrectVariablesValuesStructure();
        Assert.assertTrue(structures.incorrectVariablesValuesErrorMessageDisplay());
    }

    @Epic(value = "Structures-page")
    @Feature(value = "edit Structure")
    @Step("для недопустимых типов значений переменных отображаются сообщения об ошибке")
    @Test
    public void incorrectVariablesValuesTypeStructure() {
        structures.makeNewStructure();
        structures.incorrectVariablesValuesTypeStructure();
        Assert.assertTrue(structures.incorrectVariablesValuesTypeErrorMessageDisplay());
    }

    @Epic(value = "Structures-page")
    @Feature(value = "add Sample Points to Structure")
    @Step("Sample Points добавляются и отображаются")
    @Test
    public void addSamplePointsToNewStructure() {
        structures.makeNewStructure();
        structures.addSamplePointsToNewStructure();
        Assert.assertTrue(structures.samplePointsInNewStructureSavedCorrectly());
    }

}
