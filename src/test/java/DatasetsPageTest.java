import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ByIdOrName;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Tag("Datasets")
public class DatasetsPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Datasets datasets;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        datasets = new Datasets(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient();
        driver.quit();
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "make new Weather Dataset")
    @Step("введенные значения в новом датасете сохраняются")
    @Test
    public void makeNewWeatherDataset() {
        datasets.makeNewWeatherDataset();
        Assert.assertTrue(datasets.newWeatherDatasetDataDisplayed());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "make new Production Dataset")
    @Step("введенные значения в новом датасете сохраняются")
    @Test
    public void makeNewProductionDataset() {
        datasets.makeNewProductionDataset();
        Assert.assertTrue(datasets.newProductionDatasetDataDisplayed());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "error messages displaying")
    @Step("отображаются сообщения об ошибках при сохранении пустого датасета")
    @Test
    public void makeNewBlankDataset() {
        datasets.makeNewBlankDataset();
        Assert.assertTrue(datasets.blankDatasetErrorDisplayed());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "error messages displaying")
    @Step("отображаются сообщения при загрузке датасет-Экселя с ошибкой")
    @Test
    public void makeNewDatasetIncorrectExcel() {
        datasets.makeNewDatasetIncorrectExcel();
        Assert.assertTrue(datasets.IncorrectExcelErrorMessageDisplayed());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "edit Weather Dataset")
    @Step("значения в датасете сохраняются после редактирования")
    @Test
    public void editWeatherDataset() {
        datasets.makeNewWeatherDataset();
        datasets.editWeatherDataset();
        Assert.assertTrue(datasets.editedWeatherDatasetValuesSaved());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "edit Production Dataset")
    @Step("значения в датасете сохраняются после редактирования")
    @Test
    public void editProductionDataset() {
        datasets.makeNewProductionDataset();
        datasets.editProductionDataset();
        Assert.assertTrue(datasets.editedProductionDatasetValuesSaved());
    }

    @Epic(value = "Datasets-page")
    @Feature(value = "duplicate dataset")
    @Step("датасет скопировался с тем же именем и с теми же значениями")
    @Test
    public void duplicateDataset() {
        datasets.makeNewProductionDataset();
        Assert.assertTrue(datasets.duplicatedDatasetSame());
    }

   @Epic(value = "Datasets-page")
    @Feature(value = "delete dataset")
    @Step("датасет удаляется")
    @Test
    public void deleteDataset() {
        datasets.makeNewProductionDataset();
        Assert.assertTrue(datasets.datasetDeleted());
    }

}
