import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;


@Tag("Crops")
public class CropsPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Crops crops;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        crops = new Crops(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient();
        driver.quit();
    }

    @Epic(value = "Crops-page")
    @Feature(value = "make new Crop")
    @Step("введенные значения в новом кропе отображаются и сохраняются")
    @Test
    public void makeNewCrop() {
        crops.makeNewCrop();
        Assert.assertTrue(crops.newCropSavedCorrectly());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "new Crop")
    @Step("отображение сообщений об ошибках")
    @Test
    public void newCropErrorShow() {
        Assert.assertTrue(crops.newCropErrorMessagesDisplayed());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "edit Crop")
    @Step("новые значения в отредактированном кропе отображаются и сохраняются")
    @Test
    public void editCrop() {
        crops.makeNewCrop();
        crops.editCrop();
        Assert.assertTrue(crops.editedCropSavedCorrectly());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "edit Crop")
    @Step("отображение сообщений об ошибках при редактировании кропа")
    @Test
    public void editCropErrorShow() {
        crops.makeNewCrop();
        Assert.assertTrue(crops.editCropErrorMessagesDisplayed());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "duplicate Crop")
    @Step("кроп сдублировался со значениями кропа-оригинала")
    @Test
    public void duplicateCrop() {
        crops.makeNewCrop();
        Assert.assertTrue(crops.duplicatedCropSameValuesVariables());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "delete Crop")
    @Test
    public void deleteCrop() {
        crops.makeNewCrop();
        Assert.assertTrue(crops.cropDeleted());
    }

    @Epic(value = "Crops-page")
    @Feature(value = "delete Period")
    @Test
    public void deletePeriod() {
        crops.makeNewCrop();
        Assert.assertTrue(crops.periodDeleted());
    }

}
