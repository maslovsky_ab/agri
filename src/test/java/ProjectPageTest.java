import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.json.JSONException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;


@Tag("Projects")
public class ProjectPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Structures structures;
    Crops crops;
    Datasets datasets;
    Simulations simulations;
    Batch batch;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        structures = new Structures(driver, wait);
        crops = new Crops(driver, wait);
        datasets = new Datasets(driver, wait);
        simulations = new Simulations(driver, wait);
        batch = new Batch(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        clients.makeNewClient();
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient();
        driver.quit();
    }

  @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Test
    public void makeNewProject() {
        projects.makeNewProject();
        Assert.assertTrue(projects.newProjectMade());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Имя' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankNameField() {
        Assert.assertTrue(projects.blankNameFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Latitude' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankLatitudeField() {
        Assert.assertTrue(projects.blankLatitudeFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("поле 'Longitude' пустое - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectBlankLongitudeField() {
        Assert.assertTrue(projects.blankLongitudeFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Latitude <'-90' - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLatitudeLess90() {
        Assert.assertTrue(projects.latitudeLess90ErrorMessageDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Latitude >'+90' - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLatitudeMore90() {
        Assert.assertTrue(projects.latitudeMore90ErrorMessageDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Longitude <'-180' - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLongitudeLess180() {
        Assert.assertTrue(projects.longitudeLess180ErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "make new Project")
    @Step("Longitude >'+180' - отображение сообщения об ошибке")
    @Test
    public void makeNewProjectLongitudeMore180() {
        Assert.assertTrue(projects.longitudeMore180ErrorMessageDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("отредактированные данные сохраняются")
    @Test
    public void editProject() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editProjectCheck());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Name' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankNameField() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editBlankNameFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Latitude' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankLatitudeField() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editBlankLatitudeFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("поле 'Longitude' пустое - отображение сообщения об ошибке")
    @Test
    public void editProjectBlankLongitudeField() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editBlankLongitudeFieldErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Latitude <-90 - отображение сообщения об ошибке")
    @Test
    public void editProjectLatitudeLess90() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editProjectLatitudeLess90ErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Latitude >90 - отображение сообщения об ошибке")
    @Test
    public void editProjectLatitudeMore90() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editProjectLatitudeMore90ErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Longitude <-180 - отображение сообщения об ошибке")
    @Test
    public void editProjectLongitudeLess180() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editProjectLongitudeLess180ErrorDisplayed());
    }

    @Epic(value = "Projects-page")
    @Feature(value = "edit Project")
    @Step("Longitude >180 - отображение сообщения об ошибке")
    @Test
    public void editProjectLongitudeMore180() {
        projects.makeNewProject();
        Assert.assertTrue(projects.editProjectLongitudeMore180ErrorDisplayed());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from project page")
    @Test
    public void deleteProject() {
        projects.makeNewProject();
        Assert.assertTrue(projects.projectDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from structure page")
    @Test
    public void deleteProjectFromStructure() {
        projects.makeNewProject();
        projects.openProject();
        Assert.assertTrue(projects.projectFromStructureDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit structure page")
    @Test
    public void deleteProjectFromEditStructure() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        Assert.assertTrue(projects.projectFromEditStructureDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from crop page")
    @Test
    public void deleteProjectFromCrop() {
        projects.makeNewProject();
        projects.openProject();
        Assert.assertTrue(projects.projectFromCropDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit structure page")
    @Test
    public void deleteProjectFromEditCrop() {
        projects.makeNewProject();
        projects.openProject();
        crops.makeNewCrop();
        Assert.assertTrue(projects.projectFromEditCropDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from dataset page")
    @Test
    public void deleteProjectFromDataset() {
        projects.makeNewProject();
        projects.openProject();
        Assert.assertTrue(projects.projectFromDatasetDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit weather dataset page")
    @Test
    public void deleteProjectFromEditWeatherDataset() {
        projects.makeNewProject();
        projects.openProject();
        datasets.makeNewWeatherDataset();
        Assert.assertTrue(projects.projectFromEditWeatherDatasetDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit production dataset page")
    @Test
    public void deleteProjectFromEditProductionDataset() {
        projects.makeNewProject();
        projects.openProject();
        datasets.makeNewProductionDataset();
        Assert.assertTrue(projects.projectFromEditProductionDatasetDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from simulation page")
    @Test
    public void deleteProjectFromSimulation() {
        projects.makeNewProject();
        projects.openProject();
        Assert.assertTrue(projects.projectFromSimulationDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit simulation page")
    @Test
    public void deleteProjectFromEditSimulation() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        Assert.assertTrue(projects.projectFromEditSimulationDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from run simulation page")
    @Test
    public void deleteProjectFromSimulationResult() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        simulations.runSimulation();
        Assert.assertTrue(projects.projectFromSimulationResultDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from batch page")
    @Test
    public void deleteProjectFromBatch() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        batch.makeNewBatch();
        Assert.assertTrue(projects.projectFromBatchDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from edit batch page")
    @Test
    public void deleteProjectFromEditBatch() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        batch.makeNewBatch();
        Assert.assertTrue(projects.projectFromEditBatchDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from batch result page")
    @Test
    public void deleteProjectFromBatchResult() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        batch.makeNewBatch();
        batch.startBatchCalculation();
        Assert.assertTrue(projects.projectFromBatchResultDeleted());
    }

    @Epic(value = "Project-page")
    @Feature(value = "delete Project from batch calculation page")
    @Test
    public void deleteProjectFromBatchCalculation() {
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        simulations.makeNewSimulation();
        batch.makeNewBatch();
        batch.startBatchCalculation();
        Assert.assertTrue(projects.projectFromBatchCalculationDeleted());
    }










    @Epic(value = "Project-page")
    @Feature(value = "client name displaying in hat of projects")
    @Test
    public void clientNameDisplaying() {
        Assert.assertTrue(projects.clientNameDisplayed());
    }

    @Epic(value = "Project-page")
    @Feature(value = "client description displaying in hat of projects")
    @Test
    public void clientDescriptionDisplaying() {
        Assert.assertTrue(projects.clientDescriptionDisplayed());
    }

    @Epic(value = "Project-page")
    @Feature(value = "sorting Project")
    @Step("сортировка проектов по возрастанию имён")
    @Test
    public void sortingProjectsNameAscendingOrder() {
        projects.makeMultipleProjects();
        projects.sortingNameAscending();
        Assert.assertTrue(projects.projectsNameSortedAscending());
    }

    @Epic(value = "Project-page")
    @Feature(value = "sorting Project")
    @Step("сортировка проектов по убыванию имён")
    @Test
    public void sortingProjectsNameDescendingOrder() {
        projects.makeMultipleProjects();
        projects.sortingNameDescending();
        Assert.assertTrue(projects.projectsNameSortedDescending());
    }

    /*///////////////////////////////////// по дате сортировка работает неправильно ////////////////////
    @Epic(value = "Project-page")
    @Feature(value = "sorting Project")
    @Step("сортировка проектов по возрастанию даты изменений")
    @Test
    public void sortingProjectsDateAscendingOrder() throws JSONException, IOException {
        projects.makeMultipleProjects();
        projects.sortingDateAscending();
        Assert.assertTrue(clients.projectsDateSortedAscending());
    }
///////////////////////////////////// по дате сортировка работает неправильно ////////////////////

    @Epic(value = "Project-page")
    @Feature(value = "sorting Project")
    @Step("сортировка проектов по убыванию даты изменений")
    @Test
    public void sortingProjectsDateDescendingOrder() {
        projects.makeMultipleProjects();
        projects.sortingDateDescending();
        Assert.assertTrue(projects.projectsDateSortedDescending());
    }
/////////////////////////////////////////////////////////////////////////////////////
    */
}
