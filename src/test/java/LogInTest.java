import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Tag("Login")
public class LogInTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1280,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1280,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        logIn.loginPage(driver);
    }

    @AfterEach
    public void afterTest() {
        driver.quit();
    }

    @Epic(value = "Login-page")
    @Feature(value = "Login with correct credentials")
    @Test
    public void loginCorrect() {
        logIn.enterUsernameAndPassword("admin", "123123");
        Assert.assertTrue(logIn.checkCorrectLogin());

    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пользователь отсутствует в базе")
    @Test
    public void loginWithIncorrectUsername() {
        logIn.enterUsernameAndPassword("admin1", "123123");
        Assert.assertTrue(logIn.incorrectUsernameMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("некорректный пароль существующего юзера")
    @Test
    public void loginWithIncorrectPassword() {
        logIn.enterUsernameAndPassword("admin", "12345678");
        Assert.assertTrue(logIn.incorrectPasswordMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пустое поле имени пользователя")
    @Test
    public void loginWithBlankUsernameField() {
        logIn.enterUsernameAndPassword("", "123123");
        Assert.assertTrue(logIn.blankUsernameFieldMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("пустое поле пароля")
    @Test
    public void loginWithBlankPasswordField() {
        logIn.enterUsernameAndPassword ("admin", "");
        Assert.assertTrue(logIn.blankPasswordFieldMessageDisplayed());
    }

    @Epic(value = "Login-page")
    @Feature(value = "Incorrect Login")
    @Step("короткий пароль")
    @Test
    public void loginWithShortPassword() {
        logIn.enterUsernameAndPassword ("admin", "123");
        Assert.assertTrue(logIn.shortPasswordErrorMessageDisplayed());
    }

}
