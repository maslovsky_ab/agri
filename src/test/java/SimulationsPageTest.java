import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

@Tag("Simulations")
public class SimulationsPageTest {
    WebDriver driver;
    WebDriverWait wait;
    Login logIn;
    Clients clients;
    Projects projects;
    Structures structures;
    Crops crops;
    Datasets datasets;
    Simulations simulations;
    ChromeOptions options = new ChromeOptions();

    @BeforeEach
    void initDriver() {
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("credentials_enable_service", false);
        options.setExperimentalOption("prefs", prefs);
        options.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--headless", "--window-size=1600,1024")).create();
        //driver = WebDriverManager.chromedriver().capabilities(options.addArguments("--window-size=1600,1024")).create();
        wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        logIn = new Login(driver, wait);
        clients = new Clients(driver, wait);
        projects = new Projects(driver, wait);
        structures = new Structures(driver, wait);
        crops = new Crops(driver, wait);
        datasets = new Datasets(driver, wait);
        simulations = new Simulations(driver, wait);
        logIn.loginPage(driver);
        logIn.enterUsernameAndPassword("admin", "123123");
        clients.makeNewClient();
        projects.makeNewProject();
        projects.openProject();
        structures.makeNewStructure();
        crops.makeNewCrop();
        datasets.makeNewWeatherDataset();
        datasets.makeNewProductionDataset();
        //driver.get("http://agrisoleo.groupbwt.com/clients/695/projects/586/structures");
    }

    @AfterEach
    public void afterTest() {
        clients.deleteClient();
        //driver.quit();
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "make new Simulation")
    @Step("в новой симуляции отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void makeNewSimulation() {
        simulations.makeNewSimulation();
        simulations.readStructureData();
        simulations.readCropData();
        simulations.readWeatherDatasetData();
        simulations.readProductionDatasetData();
        Assert.assertTrue(simulations.newSimulationCreatedCorrectly());
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "edit Simulation")
    @Step("в отредактированной симуляции отображаются и сохраняются значения из Структур, Кропов и Датасетов")
    @Test
    public void editSimulation() throws InterruptedException {
        simulations.makeNewSimulation();
        simulations.editSimulation();
        Assert.assertTrue(simulations.editedSimulationSavedCorrectly());
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "duplicate Simulation")
    @Step("в копии симуляции сохраняются из симуляции-оригинала")
    @Test
    public void duplicateSimulation() {
        simulations.makeNewSimulation();
        simulations.duplicateSimulation();
        Assert.assertTrue(simulations.simulationDuplicatedCorrectly());
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "delete Simulation")
    @Step("удаление симуляции со страницы Симуляций")
    @Test
    public void deleteSimulation() {
        simulations.makeNewSimulation();
        simulations.duplicateSimulation();
        simulations.deleteSimulation();
        Assert.assertTrue(simulations.simulationDeleted());
    }

    @Epic(value = "Simulations-page")
    @Feature(value = "delete Simulation")
    @Step("удаление симуляции со страницы редактирования Симуляции")
    @Test
    public void deleteEditSimulation() {
        simulations.makeNewSimulation();
        simulations.duplicateSimulation();
        simulations.deleteSimulationFromEdit();
        Assert.assertTrue(simulations.simulationDeleted());
    }

}


